<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Validator;

/**
 * Description of LoginValidator
 *
 * @author gianmenezes
 */
class LoginValidator extends BaseValidator{
    
    protected function _txtemail($field, $value)
    {
        if ($this->isEmptyString($value)) {
            $this->addMessage($field, 'VALIDATION_REQUIRED_FIELD', compact('field'));
        }

        if (strlen(trim($value)) > 1) {
            $chars = 100;
            $this->addMessage($field,'VALIDATION_MAXIMUM_CHARS', compact('field', 'chars'));
        }
    }
    
    protected function _txtpassword($field, $value)
    {
        if ($this->isEmptyString($value)) {
            $this->addMessage($field, 'VALIDATION_REQUIRED_FIELD', compact('field'));
        }

        if (strlen(trim($value)) > 1) {
            $chars = 100;
            $this->addMessage($field,'VALIDATION_MAXIMUM_CHARS', compact('field', 'chars'));
        }
    }
    
    
    
}
