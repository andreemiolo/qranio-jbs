<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Validator;

/**
 * Description of BaseValidator
 *
 * @author gianmenezes
 */
class BaseValidator{

    /**
     * @var Slim\Container
     */
    protected $container;
    
    /**
     * @var array
     */
    protected $messages = [];

    public function __construct($container) {
        // make the container available in the class
        $this->container = $container;
    }
    
    /**
     * Adiciona uma mensagem de erro
     * @param string $field Nome do campo que contém o erro
     * @param string $message Mensagem a ser gravada
     * @return \app\validator\BaseValidator
     */
    protected function addMessage($field, $message)
    {
        $this->messages['FIELD_' . strtoupper($field)][] = $message;
        return $this;
    }

    /**
     * Retorna a lista com as mensagens de erro
     * @return array
     */
    public function getMessages()
    {
        return $this->messages;
    }
    
    /**
     * Executa a validação
     * @param array $data
     * @return bool
     */
    public function validate(array $data)
    {
        $this->data = $data;

        foreach ($this->data as $key => $value) {

            $method = '_' . $key;

            if (!method_exists($this, $method)) {
                throw new \Exception('Metodo nao existe '.$method, 500);
            }

            $this->$method($key, $value);
        }

        return count($this->messages) === 0;
    }
    
    /**
     * Retorna o valor de um campo, contido no array data que foi passado para o método validate()
     * @param string $name Nome do campo
     * @param mixed $default Valor a ser retornado caso o campo não exista
     * @return mixed
     */
    protected function getField($name, $default = null)
    {
        return array_key_exists($name, $this->data) ? $this->data[$name] : $default;
    }

    /**
     * Verifica se uma string é vazia
     * @param string $value
     * @return bool
     */
    protected function isEmptyString($value)
    {
        return strlen(trim($value)) === 0;
    }

    /**
     * Verifica se uma string contém somente letras e espaço
     * @param string $value
     * @return bool
     */
    protected function isOnlyLettersAndSpace($value)
    {
        return preg_match('/^[a-zA-Z ]+$/msi', $value);
    }
    /**
     * Verifica se uma string contém somente letras e espaço
     * @param string $value
     * @return bool
     */
    protected function isEmail($value)
    {
        return preg_match('/^[a-zA-Z ]+$/msi', $value);
    }

    

}
