<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Library;

use SendGrid;

/**
 * Description of Email
 *
 * @author gianmenezes
 */
class Email {

    /**
     * @var Slim\Container
     */
    protected $container;

    public function __construct($container) {
        $this->container = $container;
    }

    public function sendMail($param) {

        $from = new SendGrid\Email("Example User", "test@example.com");
        $subject = "Sending with SendGrid is Fun";
        
        $to = new SendGrid\Email("Example User", "test@example.com");
        
        $content = new \SendGrid\Content("text/plain", "and easy to do anywhere, even with PHP");
        
        
        $mail = new SendGrid\Mail($from, $subject, $to, $content);
        
        $apiKey = getenv('SENDGRID_API_KEY');
        
        
        $sg = new \SendGrid($apiKey);
        $response = $sg->client->mail()->send()->post($mail);
        echo $response->statusCode();
        print_r($response->headers());
        echo $response->body();
    }

}
