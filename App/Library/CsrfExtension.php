<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Library;

/**
 * Description of CsrfExtension
 *
 * @author gianmenezes
 */
class CsrfExtension extends \Twig_Extension implements \Twig_Extension_GlobalsInterface {

    /**
     * @var \Slim\Csrf\Guard
     */
    protected $csrf;

    public function __construct(\Slim\Csrf\Guard $csrf) {
        $this->csrf = $csrf;
    }

    public function getGlobals() {
        // CSRF token name and value
        $csrfNameKey = $this->csrf->getTokenNameKey();
        $csrfValueKey = $this->csrf->getTokenValueKey();
        $csrfName = $this->csrf->getTokenName();
        $csrfValue = $this->csrf->getTokenValue();

        return [
            'csrf' => [
                'keys'  => [
                    'name'  => $csrfNameKey,
                    'value' => $csrfValueKey
                ],
                'name'  => $csrfName,
                'value' => $csrfValue
            ]
        ];
    }

    public function getName() {
        return 'slim/csrf';
    }

}
