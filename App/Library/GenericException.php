<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Library;

/**
 * Description of GenericException
 *
 * @author gianmenezes
 */
class GenericException extends \Exception {

    protected $isValidation = false;
    protected $messages = [];

    public function setMessages(array $messages) {
        $this->messages = $messages;
        return $this;
    }

    public function getMessages() {
        return $this->messages;
    }

    public function setIsValidation($value) {
        $this->isValidation = $value;
        return $this;
    }

    public function getIsValidation() {
        return $this->isValidation;
    }

}
