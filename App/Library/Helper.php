<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Library;

/**
 * Description of Helper
 *
 * @author gianmenezes
 */
class Helper {

    /**
     * @var \Slim\Container
     */
    protected $container;

    public function __construct($container) {
        $this->container = $container;
    }

    /**
     * Verifica se a sessão está ativa e o usuário está autenticado
     * @return boolean
     */
    public function isAuthenticated() {
        $check = $this->container['session']->get('authqr', false);
        if (!$check) {
            return false;
        }
        return true;
    }

    /**
     * Retorna um elemento de formulário
     * @param \Aura\Input\Form $form Instância do formulário
     * @param string $name Nome do campo no formulário
     * @return string
     */
    public function field(\Aura\Input\Form $form, $name) {
        $factory = new \Aura\Html\HelperLocatorFactory;
        $helper_input = $factory->newInstance();
        return $helper_input->input($form->get($name));
    }

    function showFieldErrors(\Aura\Input\Form $form, $name) {
        $errors = $form->getMessages($name);
        $str = '';
        if ($errors) {
            foreach ($errors as $error) {
            $str .= '<label id="'.$name.'-error" for="'.$name.'" class="error">' . $this->qrTranslate($error) . '</label>';
            }
        }
        return $str;
    }

    /**
     * Traduz uma mensagem
     * @param string $name
     * @param array $params
     * @return string
     */
    function qrTranslate($name, array $params = []) {
        $_params = [];

        foreach ($params as $k => $v) {
            $_params["%{$k}%"] = $v;
        }

        return $this->container['translator']->trans($name, $_params);
    }
    
    /**
     * Encrypt passwprd
     * @param string $password
     * @return string
     */
    function qrEcryptPassword($password) {
        
        $pass = hash('sha256', md5($password));
        return $pass;
        
    }

    /**
     * Criptografa um ID (número inteiro)
     * @param int $id
     * @return string
     */
    public function idEncrypt($id) {
        return $this->container['hasher']->encode($id);
    }

    /**
     * Descriptografa um hash
     * @param string $hash
     * @return int
     */
    public function idDecrypt($hash) {

        $id = $this->container['hasher']->decode($hash);

        return is_array($id) && isset($id[0]) ? $id[0] : null;
    }


    /**
     * Cria a URL vinda do JS
     * @param string $hash
     * @return int
     */
    public function createURL($path_for, $id)
    {


        $id = $this->container['hasher']->decode($hash);

        return is_array($id) && isset($id[0]) ? $id[0] : null;
    }

    /**
     * Retorna os menus
     * @return array
     */
    public function getMenu() {
        return $this->container['session']->get('menu');
    }

    /**
     * Retorna se o usuário pode acessar uma rota específica
     * @param string $route Rota a ser verificada
     * @return boolean Retorna true se o usuário pode acessar a rota
     */
    public function userCanAccessRoute($route) {
        return ($this->container['session']->get('full_access') === 'Y' || in_array($route, $this->container['session']->get('route_allowed')));
    }

    /**
     * Converte uma data de um formato para outro
     * @param string $date Data a ser convertida
     * @param string $originFormat Formato de origem
     * @param string $destinyFormat Formato destino
     * @return string Data convertida
     */
    public function convertDate($date, $originFormat, $destinyFormat) {
        $d = \DateTime::createFromFormat($originFormat, $date);
        return $d->format($destinyFormat);
    }

    /**
     * Retorna se uma rota é válida
     * @param $route
     * @return boolean indica se rota é válida
     */
    public function routeIsValid($route) {
        //syslog(LOG_ERR, 'Entrou no método a rota ' . $route);
        $valid = false;
        try {
            $r = $this->container['router']->pathFor($route);
            if ($r) {
                $valid = true;
                //syslog(LOG_ERR, 'Pegou o caminho válido "' . $r . '" da rota ' . $route);
            }
            else {
                //syslog(LOG_ERR, 'Pegou o caminho inválido "' . $r . '" da rota ' . $route);
            }
        }
        catch(\Exception $e) {
            //Não faço nada, só para evitar as exceções que podem invalidar a rota
            //syslog(LOG_ERR, 'Não pegou o caminho da rota ' . $route . ' pois deu erro: ' . $e->getMessage());
        }
        return $valid;
    }

    /**
     * Remove menus inválidos, vai ser util quando tiver mais de uma pessoa desenvolvendo, não deve ser utilizado na produção
     * @param array $menus $menus que vieram do WS
     * @return array Menus sem rotas inválidas
     */
    public function removeInvalidRoutesFromMenu(array $menus) {
        syslog(LOG_ERR, 'Entrada: ' . json_encode($menus));
        $return = [];
        foreach($menus as $m) {
            if ($m['route']) {
                if ($this->routeIsValid($m['route'])) {
                    array_push($return, $m);
                }
            } else {
                if ($m['children']) {
                    $copy = $m;
                    $children = $this->removeInvalidRoutesFromMenu($m['children']);
                    if ($children) {
                        $copy['children'] = $children;
                        array_push($return, $copy);
                    }
                }
            }
        }
        syslog(LOG_ERR, 'Saída: ' . json_encode($return));
        return $return;
    }

    //<editor-fold desc="Métodos para preencher selects">
    /**
     * Retorna a lista de Grupos de Usuarios para utilizar no Select de groups
     * @return array
     */
    public function listOptionsGroupsCerebelo() {
        $service = new \App\Http\Service\AdministratorService($this->container);

        $wsData    = $service->listGroupsCerebelo(['id_user' => $this->container['session']->get('user_id')]);
        $groups    = $wsData['data']['groups'];
        $arrayGroups = [];
        foreach ($groups as $group) {
            $arrayGroups[$group['id_group']] = $group['title'];
        }
        return $arrayGroups;
    }

    /**
     * Retorna a lista de Grupos de Usuarios para utilizar no Select de groups
     * @return array
     */
    public function listOptionsSupervisedUsers() {
        $service = new \App\Http\Service\TaskService($this->container);

        $wsData    = $service->listSupervisedUsers(['id_user' => $this->container['session']->get('user_id')]);
        $users    = $wsData['data']['supervised_users'];
        $return = [];
        foreach ($users as $u) {
            $return[$u['id_user']] = $u['user_name'];
        }
        return $return;
    }

    /**
     * Retorna a lista de Idiomas para utilizar no Select de Escolha de Idioma
     * @return array
     */
    public function listOptionsLanguage() {
        return [
            'pt_BR' => $this->qrTranslate('OPTION_PORTUGUESE_BR'),
            //'es_ES' => $this->qrTranslate('OPTION_SPANISH'),
            'en_US' => $this->qrTranslate('OPTION_ENGLISH'),
            //'pt_PT' => $this->qrTranslate('OPTION_PORTUGUESE_PT'),
            //'zh_CN' => $this->qrTranslate('OPTION_CHINESE')
        ];
    }

    /**
     * Retorna a lista de Tipos de permissão de tarefas
     * @return array
     */
    public function listOptionsTaskPermissionTypes() {
        return [
            'D' => $this->qrTranslate('OPTION_TASK_PERMISSION_D'),
            'T' => $this->qrTranslate('OPTION_TASK_PERMISSION_T'),
        ];
    }

    /**
     * Retorna a lista de usuários habilitados no cerebelo
     * @return array
     */
    public function listOptionsEnabledUsers() {
        $service = new \App\Http\Service\AdministratorService($this->container);

        $wsData    = $service->listUsers(['id_user' => $this->container['session']->get('user_id'), 'filter' => "ur.Habilitar != 'N'"]);
        $users    = $wsData['data']['users'];
        $return = [];
        foreach ($users as $u) {
            $return[$u['id_user']] = $u['name'];
        }
        return $return;
    }

    /**
     * Retorna a lista de status de tarefa de acordo com o período
     * @return array
     */
    public function listOptionsTaskIntervalStatus() {
        return [
            ['translation_key' => 'TASK_STATUS_LATE', 'class' => 'label label-danger'],
            ['translation_key' => 'TASK_STATUS_ON_TIME', 'class' => 'label label-warning'],
            ['translation_key' => 'TASK_STATUS_FUTURE', 'class' => 'label label-white'],
            ['translation_key' => 'TASK_STATUS_ENDED', 'class' => 'label label-success'],
            ['translation_key' => 'TASK_STATUS_WAITING_OPERATOR', 'class' => 'label label-primary'],
        ];
    }

    /**
     * Retorna a lista de status de tarefa
     * @return array
     */
    public function listOptionsTaskStatus() {
        return [
            'A' => $this->qrTranslate('TASK_STATUS_OPEN'),
            'E' => $this->qrTranslate('TASK_STATUS_ENDED'),
            'C' => $this->qrTranslate('TASK_STATUS_WAITING'),
        ];
    }
    //</editor-fold>
}
