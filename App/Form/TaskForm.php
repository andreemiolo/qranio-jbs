<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Form;

use App\Library\Helper;
use Slim\Container;

/**
 * Description of TaskForm
 *
 * @author davi
 */
class TaskForm extends BaseForm
{

    public function init()
    {

        $filter = $this->getFilter();
        $options = $this->getOptions();

        $this->setField('id_task', 'hidden');
        $this->setField('id_operator', 'select')
            ->setAttribs(array(
                'id'    => 'id_operator',
                'class' => 'form-control select2',
                'required' => '',
                'style' => 'width: 100%;'
            ))
            ->setOptions(
                $options['helper']->listOptionsEnabledUsers()
            );

        $filter->setRule('id_operator', 'VALIDATOR_EMPTY', function ($value) {
            return $this->validateEmpty($value);
        });

        $this->setField('id_executor', 'select')
            ->setAttribs(array(
                'id'    => 'id_executor',
                'class' => 'form-control select2',
                'required' => ''
            ))
            ->setOptions(
                $options['helper']->listOptionsSupervisedUsers()
            );

        $this->setField('name', 'text')
            ->setAttribs([
                'class' => 'form-control',
                'maxlength' => 255,
                'required' => ''
            ]);

        $filter->setRule('name', 'VALIDATOR_EMPTY', function ($value) {
            return $this->validateEmpty($value);
        });

        $this->setField('description', 'textarea')
            ->setAttribs([
                'id' => 'description',
                'class' => 'form-control',
                'rows'  => '3',
                'required' => '',
            ]);

        $filter->setRule('description', 'VALIDATOR_EMPTY', function ($value) {
            return $this->validateEmpty($value);
        });

        $this->setField('task_initial_date', 'text')
            ->setAttribs([
                'class' => 'form-control',
                'id'        => 'task_initial_date',
                'required' => '',
                'readonly' => '',
            ]);

        $filter->setRule('task_initial_date', 'VALIDATOR_EMPTY', function ($value) {
            return $this->validateEmpty($value);
        });

        $this->setField('task_final_date', 'text')
            ->setAttribs(array(
                'class'     => 'form-control',
                'id'        => 'task_final_date',
                'required' => '',
                'readonly' => '',
            ));

        $filter->setRule('task_final_date', 'VALIDATOR_TASK_FINAL_DATE', function ($value, $fields) {
            $initial_date = \DateTime::createFromFormat('d/m/Y H:i', $fields->task_initial_date);
            $final_date = \DateTime::createFromFormat('d/m/Y H:i', $value);
            return ($final_date->format('Y-m-d H:i') > $initial_date->format('Y-m-d H:i'));
        });

    }

}
