<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Form;

/**
 * Description of LoginForm
 *
 * @author gianmenezes
 */
class LoginForm extends BaseForm {

    public function init() {

        $filter = $this->getFilter();

        $this->setField('email', 'text')
                ->setAttribs([
                    'class'    => 'form-control',
                    'required' => ''
        ]);
        $filter->setRule(
                'email', 'VALIDATOR_EMPTY', function ($value) {
            return $this->validateEmpty($value);
        });


        $this->setField('password', 'password')
                ->setAttribs([
                    'class'    => 'form-control',
                    'required' => ''
        ]);
        $filter->setRule(
                'password', 'VALIDATOR_EMPTY', function ($value) {
            return $this->validateEmpty($value);
        });
    }

}
