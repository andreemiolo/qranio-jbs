<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Form;

use App\Library\Helper;
use Slim\Container;

/**
 * Description of AdministratorTaskPermissionForm
 *
 * @author davi
 */
class AdministratorTaskPermissionForm extends BaseForm
{

    public function init()
    {

        $filter = $this->getFilter();
        $options = $this->getOptions();

        $this->setField('id_supervisor', 'hidden');

        $filter->setRule(
            'id_supervisor', 'VALIDATOR_EMPTY', function ($value) {
            return $this->validateEmpty($value);
        });

        $this->setField('id_executor', 'select')
            ->setAttribs([
                'id' => 'id_executor',
                'class' => 'form-control select2',
                'required' => ''
            ]);

        $filter->setRule(
            'id_executor', 'VALIDATOR_EMPTY', function ($value) {
            return $this->validateEmpty($value);
        });

        $this->setField('task_permission_type', 'select')
            ->setAttribs([
                'id' => 'task_permission_type',
                'class' => 'form-control select2',
                'required' => ''
            ])->setOptions($options['helper']->listOptionsTaskPermissionTypes());

        $filter->setRule(
            'task_permission_type', 'VALIDATOR_EMPTY', function ($value) {
            return $this->validateEmpty($value);
        });

    }

}
