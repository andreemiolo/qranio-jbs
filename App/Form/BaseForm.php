<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Form;

use Aura\Input\Form;

/**
 * Description of BaseForm
 *
 * @author gianmenezes
 */
class BaseForm extends Form {


    protected function validateEmpty($value, $zero_allow = false) {
        if (empty($value)) {
            if (is_numeric($value) && $zero_allow && (string) $value === '0') {
                return true;
            }
            return false;
        }
        if (is_string($value) && empty(trim($value))) {
            return false;
        }
        return true;
    }

    protected function validateMaxLength($value, $max_length) {
        if (is_string($value) && mb_strlen($value, 'UTF-8') > $max_length) {
            return false;
        }
        return true;
    }

    protected function validateEmail($value) {
        if (empty($value) || !filter_var($value, FILTER_VALIDATE_EMAIL)) {
            return false;
        }
        return true;
    }

}
