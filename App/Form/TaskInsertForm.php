<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Form;

use App\Library\Helper;
use Slim\Container;

/**
 * Description of TaskForm
 *
 * @author davi
 */
class TaskInsertForm extends TaskForm
{

    public function init()
    {
        parent::init();

        $filter = $this->getFilter();

        $filter->setRule('id_executor', 'VALIDATOR_EMPTY', function ($value) {
            return $this->validateEmpty($value);
        });
    }

}
