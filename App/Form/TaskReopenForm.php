<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Form;

use App\Library\Helper;
use Slim\Container;

/**
 * Description of TaskForm
 *
 * @author davi
 */
class TaskReopenForm extends TaskProgressForm
{

    public function init()
    {
        parent::init();

        $filter = $this->getFilter();

        $filter->setRule('task_reopen_initial_date', 'VALIDATOR_EMPTY', function ($value) {
            return $this->validateEmpty($value);
        });

        $filter->setRule('task_reopen_final_date', 'VALIDATOR_TASK_FINAL_DATE', function ($value, $fields) {
            $initial_date = \DateTime::createFromFormat('d/m/Y H:i', $fields->task_reopen_initial_date);
            $final_date = \DateTime::createFromFormat('d/m/Y H:i', $value);
            return ($final_date->format('Y-m-d H:i') > $initial_date->format('Y-m-d H:i'));
        });

    }

}
