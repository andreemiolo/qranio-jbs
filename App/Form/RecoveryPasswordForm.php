<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Form;

/**
 * Description of LoginForm
 *
 * @author gianmenezes
 */
class RecoveryPasswordForm extends BaseForm {

    public function init() {

        $filter = $this->getFilter();

        $this->setField('email', 'email')
                ->setAttribs([
                    'class'       => 'form-control',
                    'placeholder' => qrTranslate('LABEL_EMAIL'),
                    'required'    => ''
        ]);

        $filter->setRule(
                'email', 'VALIDATOR_EMAIL', function ($value) {
            return $this->validateEmail($value);
        });
    }

}
