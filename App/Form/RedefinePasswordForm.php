<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Form;

/**
 * Description of TemporaryPasswordForm
 *
 * @author Davi
 */
class RedefinePasswordForm extends BaseForm {

    public function init() {

        $filter = $this->getFilter();
        $this->setField('id_user', 'hidden');
        $this->setField('expired', 'hidden');

        $this->setField('password', 'password')
                ->setAttribs([
                    'class'    => 'form-control',
                    'required' => ''
        ]);
        $filter->setRule('password', 'VALIDATOR_PASSWORD', function ($value) {
            if (!$this->validateEmpty($value) ) {
                return false;
            }
            if (strlen(trim($value)) < 8) {
                return false;
            }
            if (!preg_match('/[a-zA-Z]/', $value)) {
                return false;
            }
            if (!preg_match('/[0-9]/', $value)) {
               return false;
            }
            return true;
        });

        $this->setField('confirm_password', 'password')
            ->setAttribs([
                'class'    => 'form-control',
                'required' => ''
            ]);
        $filter->setRule('confirm_password', 'VALIDATOR_CONFIRM_PASSWORD', function ($value, $fields) {
            return ($value == $fields->password);
        });
    }

}
