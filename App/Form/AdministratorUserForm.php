<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Form;

use App\Library\Helper;
use Slim\Container;

/**
 * Description of AdministratorUserGroupForm
 *
 * @author gianmenezes
 */
class AdministratorUserForm extends BaseForm
{

    public function init()
    {

        $filter = $this->getFilter();
        $options = $this->getOptions();

        $this->setField('id_user', 'hidden');

        $this->setField('photo', 'hidden')
            ->setAttribs(array(
                'id'    => 'photo',
                'value' => '',
            ));

        $this->setField('fname', 'text')
            ->setAttribs([
                'class' => 'form-control',
                'maxlength' => 255,
                'required' => ''
            ]);

        $filter->setRule(
            'fname', 'VALIDATOR_EMPTY', function ($value) {
            return $this->validateEmpty($value);
        });

        $this->setField('lname', 'text')
            ->setAttribs([
                'class' => 'form-control',
                'maxlength' => 255,
                'required' => ''
            ]);

        $filter->setRule(
            'lname', 'VALIDATOR_EMPTY', function ($value) {
            return $this->validateEmpty($value);
        });

        $this->setField('occupation', 'text')
            ->setAttribs([
                'class' => 'form-control',
                'maxlength' => 255,
                'required' => '',
            ]);

        $filter->setRule(
            'occupation', 'VALIDATOR_EMPTY', function ($value) {
            return $this->validateEmpty($value);
        });

        $this->setField('email', 'text')
            ->setAttribs(array(
                'maxlength' => 150,
                'class'     => 'form-control',
                'id'        => 'email',
                'required' => ''
            ));

        $filter->setRule(
            'email', 'VALIDATOR_EMAIL', function ($value) {
            return $this->validateEmail($value);
        });

        $this->setField('id_group', 'select')
            ->setAttribs(array(
                'id'    => 'id_group',
                'class' => 'form-control select2',
                'required' => ''
            ))
            ->setOptions(
                $options['helper']->listOptionsGroupsCerebelo()
            );

        $filter->setRule(
            'id_group', 'VALIDATOR_EMPTY', function ($value) {
            return $this->validateEmpty($value, true);
        });

        $this->setField('language', 'select')
            ->setAttribs(array(
                'id' => 'language',
                'class' => 'form-control select2',
                'required' => '',
            ))
            ->setOptions(
                $options['helper']->listOptionsLanguage()
            );

        $filter->setRule(
            'language', 'VALIDATOR_EMPTY', function ($value) {
            return $this->validateEmpty($value);
        });

        $this->setField('enable', 'select')
            ->setAttribs([
                'id' => 'enable',
                'class' => 'form-control select2',
                'required' => ''
            ])->setOptions(
                listSelectOptions(['Y','N'])
            );

        $filter->setRule(
            'enable', 'VALIDATOR_EMPTY', function ($value) {
            return $this->validateEmpty($value);
        });

    }

}
