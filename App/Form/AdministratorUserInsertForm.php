<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Form;

use App\Library\Helper;
use Slim\Container;

/**
 * Description of AdministratorUserGroupForm
 *
 * @author gianmenezes
 */
class AdministratorUserInsertForm extends AdministratorUserForm
{

    public function init()
    {
        parent::init();
        $filter = $this->getFilter();

        $this->setField('password', 'password')
            ->setAttribs([
                'id' => 'password',
                'class' => 'form-control',
                'maxlength' => 20,
                //'required' => '',
            ]);

        $filter->setRule(
            'password', 'VALIDATOR_EMPTY', function ($value, $fields) {
            if ($fields->password_random == 'Y') {
                return true;
            }
            return $this->validateEmpty($value);
        });

        $this->setField('password_random', 'checkbox')
            ->setAttribs(array(
                'id' => 'password_random',
                'value' => 'Y'
            ));

    }

}
