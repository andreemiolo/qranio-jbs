<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Form;

use App\Library\Helper;
use Slim\Container;

/**
 * Description of TaskForm
 *
 * @author davi
 */
class TaskProgressForm extends BaseForm
{

    public function init()
    {

        $filter = $this->getFilter();
        $options = $this->getOptions();

        $this->setField('id_task', 'hidden');
        $this->setField('id_user', 'hidden');
        $this->setField('id_current_executor', 'hidden');

        $this->setField('task_progress', 'textarea')
            ->setAttribs([
                'id' => 'task_progress',
                'class' => 'form-control',
                'rows'  => '3',
                'required' => '',
            ]);

        $filter->setRule('task_progress', 'VALIDATOR_EMPTY', function ($value) {
            return $this->validateEmpty($value);
        });

        $this->setField('id_executor', 'select')
            ->setAttribs(array(
                'id'    => 'id_executor',
                'class' => 'form-control select2',
                'required' => '',
                'style' => 'width: 100%'
            ))
            ->setOptions(
                $options['helper']->listOptionsSupervisedUsers()
            );

        $this->setField('task_reopen_initial_date', 'text')
            ->setAttribs([
                'class' => 'form-control',
                'id'        => 'task_reopen_initial_date',
                'required' => '',
                'readonly' => '',
            ]);

        $this->setField('task_reopen_final_date', 'text')
            ->setAttribs(array(
                'class'     => 'form-control',
                'id'        => 'task_reopen_final_date',
                'required' => '',
                'readonly' => '',
            ));

    }

}
