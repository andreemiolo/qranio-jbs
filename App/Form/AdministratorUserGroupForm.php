<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Form;

use App\Library\Helper;
use Slim\Container;

/**
 * Description of AdministratorUserGroupForm
 *
 * @author gianmenezes
 */
class AdministratorUserGroupForm extends BaseForm
{

    public function init()
    {

        $filter = $this->getFilter();

        $this->setField('id_group', 'hidden');

        $this->setField('title', 'text')
            ->setAttribs([
                'class' => 'form-control',
                'required' => ''
            ]);

        $filter->setRule(
            'title', 'VALIDATOR_EMPTY', function ($value) {
            return $this->validateEmpty($value);
        });

//        $filter->setRule(
//            'title', 'VALIDATOR_EMAIL', function ($value) {
//            return $this->validateEmail($value);
//        });

        $this->setField('enable', 'select')
            ->setAttribs([
                'id' => 'enable',
                'class' => 'form-control select2',
                'required' => ''
            ])->setOptions(
                listSelectOptions(['Y','N'])
            );

        $filter->setRule(
            'enable', 'VALIDATOR_EMPTY', function ($value) {
            return $this->validateEmpty($value);
        });

    }

}
