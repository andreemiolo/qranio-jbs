<?php
// Verifica se é permitido o acesso a rota
$app->add($container->get('authentication_check_middleware'));

// Application middleware
// Faz Guard (SEMPRE DEIXAR ANTES DA SESSÃO)
$app->add($container->get('csrf'));

// Adiciona Sessao
$app->add(new \Slim\Middleware\Session([
    'name' => 'cerebelo_session',
    'autorefresh' => true,
    'lifetime' => '10 minutes'
]));

// Retira Slash da url se tiver
$app->add(function ($request, $response, callable $next) {
    $uri = $request->getUri();
    $path = $uri->getPath();
    if ($path != '/' && substr($path, -1) == '/') {
        // permanently redirect paths with a trailing slash
        // to their non-trailing counterpart
        $uri = $uri->withPath(substr($path, 0, -1));
        if ($request->getMethod() == 'GET') {
            return $response->withRedirect((string)$uri, 301);
        } else {
            return $next($request->withUri($uri), $response);
        }
    }

    return $next($request, $response);
});