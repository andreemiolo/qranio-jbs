<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Middleware;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * Description of AuthenticationCheckMiddleware
 *
 * @author gianmenezes
 */
class AuthenticationCheckMiddleware {

    /**
     * @var \Slim\Container
     */
    protected $container;

    public function __construct($container) {
        // make the container available in the class
        $this->container = $container;
    }

    function __invoke(Request $request, Response $response, callable $next) {
        // mapeamento de rotas que permitem serem acessadas
        // sem estar autenticado na aplicação
        $skip = ['asset', 'index', 'login', 'recovery.password', 'new.password', 'confirm.account', 'logout'];
        $route = $request->getAttribute('route');
        if (QR_ENV == 'development') {
            syslog(LOG_ERR, 'ROTA DO CEREBELO: ' . $route->getName());
            if ($request->getMethod() == 'POST') {
                syslog(LOG_ERR, 'PARÂMETROS PASSADOS PARA A ROTA DO CEREBELO: ' . json_encode($_POST));
            }
        }

        if (!$route) {
             $msg_error = [
                'number' => 00214,
                'title'  => $this->container['helper']->qrTranslate('VALIDATOR_ERROR_CSRF_TITLE'),
                'msg'    => $this->container['helper']->qrTranslate('VALIDATOR_ERROR_CSRF_MESSAGE')
            ];
            
            $url = $this->container['router']->pathFor('error',[], $msg_error);
            return $response->withStatus(404)->withHeader('Location', $url);
        }

        $isAutenticate = $this->container['session']->get('authqr', false);

        // Verifica se a rota é o index se for e o usurio estiver logado redireciona pra Home
        if ($route && in_array($route->getName(), $skip)) {

            $routesRedirectAutenticate = ['index', 'recovery.password', 'new.password', 'confirm.account'];
            if (in_array($route->getName(), $routesRedirectAutenticate) && $isAutenticate === true) {
                $url = $this->container['router']->pathFor('home');
                return $response->withStatus(302)->withHeader('Location', $url);
            }

            // Do stuff before passing along
            $newResponse = $next($request, $response);

            // Do stuff after route is rendered
            return $newResponse; // continue
        }

        if ($isAutenticate) {
            $skip_redirect = ['asset', 'login', 'recovery.password', 'logout', 'temporary.password', 'expired.password', 'redefine.password'];
            if ($this->container['session']->get('temporary_password') === 'Y' && !in_array($route->getName(), $skip_redirect)) {
                $url = $this->container['router']->pathFor('temporary.password');
                return $response->withStatus(302)->withHeader('Location', $url);
            }

            if ($this->container['session']->get('expired_password') === 'Y' && !in_array($route->getName(), $skip_redirect)) {
                $url = $this->container['router']->pathFor('expired.password');
                return $response->withStatus(302)->withHeader('Location', $url);
            }

            if (!$this->container['helper']->userCanAccessRoute($route->getName())) {
                $url = $this->container['router']->pathFor('home');
                return $response->withStatus(403)->withHeader('Location', $url);
            }

            // Do stuff before passing along
            $newResponse = $next($request, $response);

            // Do stuff after route is rendered
            return $newResponse; // continue
        }
        else {
            $url = $this->container['router']->pathFor('index');
            return $response->withStatus(302)->withHeader('Location', $url);
        }
    }

}
