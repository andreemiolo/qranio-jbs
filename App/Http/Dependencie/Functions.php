<?php

/**
 * Traduz uma mensagem
 * @param string $name
 * @param array $params
 * @return string
 */
function qrTranslate($name, array $params = []) {
    $_params = [];

    foreach ($params as $k => $v) {
        $_params["%{$k}%"] = $v;
    }

    $settings = require QR_DIR_BASE . '/app/Http/Config/Settings.php';
    // First param is the "default language" to use.
    $translatorx = new Symfony\Component\Translation\Translator($settings['settings']['app']['language'], new Symfony\Component\Translation\MessageSelector());
    // Set a fallback language incase you don't have a translation in the default language
    $translatorx->setFallbackLocales(['pt_BR']);
    // Add a loader that will get the php files we are going to sthore our translations in
    $translatorx->addLoader('php', new Symfony\Component\Translation\Loader\PhpFileLoader());
    // Add language files here
    $translatorx->addResource('php', $settings['settings']['renderer']['translations_path'] . '/pt_BR/messages.php', 'pt_BR'); // PORTUGUES
    $translatorx->addResource('php', $settings['settings']['renderer']['translations_path'] . '/en_US/messages.php', 'en_US'); // INGLÊS
    return $translatorx->trans($name, $_params);
}


/**
 * Retorna a lista de Status para utilizar no Select de Enable
 * @return array
 */
function listSelectOptions(array $options_enable = ['']) {

    $array_opt[''] = qrTranslate('OPTION_SELECT');
    $opt = [
        'N' => qrTranslate('OPTION_NO'),
        'Y' => qrTranslate('OPTION_YES'),
    ];

    foreach ($options_enable as $k => $v) {
        $array_opt[$v] = $opt[$v];
    }

    return $array_opt;
}