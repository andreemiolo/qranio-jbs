<?php

// DIC configuration

$container = $app->getContainer();

// AuthenticationCheckMiddleware
$container['authentication_check_middleware'] = function ($c) {
    return new \App\Http\Middleware\AuthenticationCheckMiddleware($c);
};

// Register Csrf middleware
$container['csrf'] = function ($c) {
    $guard = new \Slim\Csrf\Guard();
    $guard->setFailureCallable(function ($request, $response, $next) {
        $request = $request->withAttribute("csrf_status", false);
        return $next($request, $response);
    });
    return $guard;
};

// Register Hasher
$container['hasher'] = function ($c) {
    $settings = $c->get('settings');
    return new Hashids\Hashids($settings->get('app')['salt'],16);
};
// Register Helper
$container['helper'] = function ($c) {
    return new App\Library\Helper($c);
};

// Session
$container['session'] = function ($c) {
    return new \SlimSession\Helper;
};

// Register Purifier - Limpa as strings para não ter problema com sqlinjection e outros
$container['purifier'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    $config = HTMLPurifier_Config::createDefault();
    $config->set('Cache.SerializerPath', $settings['cache_path']);
    return new HTMLPurifier($config);
};

// Register Translator
$container['translator'] = function ($c) {
    $settings = $c->get('settings');
    // First param is the "default language" to use.
    $translator = new Symfony\Component\Translation\Translator($settings->get('app')['language'], new Symfony\Component\Translation\MessageSelector());
    // Set a fallback language incase you don't have a translation in the default language
    $translator->setFallbackLocales(['pt_BR']);
    // Add a loader that will get the php files we are going to store our translations in
    $translator->addLoader('php', new Symfony\Component\Translation\Loader\PhpFileLoader());
    // Add language files here
    $translator->addResource('php', $settings->get('renderer')['translations_path'] . '/pt_BR/messages.php', 'pt_BR'); // PORTUGUES
    $translator->addResource('php', $settings->get('renderer')['translations_path'] . '/en_US/messages.php', 'en_US'); // INGLÊS
    return $translator;
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

// Register Twig View helper
$container['view'] = function ($c) {

    $settings = $c->get('settings');
    $view = new \Slim\Views\Twig($settings->get('renderer')['template_path'], [
        // 'cache' => $settings['cache_path']
    ]);

    //Adiciona a extensão do CSRF de proteção para formulários
    $view->addExtension(new App\Library\CsrfExtension($c['csrf']));

    // add translator functions to Twig
    $view->addExtension(new Symfony\Bridge\Twig\Extension\TranslationExtension($c['translator']));
    $view->getEnvironment()->addGlobal('language', $settings->get('app')['language']);

    // Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', $c['request']->getUri()->getBasePath()), '/');

    // Adiciona extensão das rotas
    $view->addExtension(new Slim\Views\TwigExtension($c['router'], $basePath));

    //Passa o Helper para que possa ser utilizado na view
    $view->offsetSet('h', $c['helper']);

    //Passa as informações da sessão para que possa ser utilizado na view
    $view->offsetSet('s', $c['session']);

    return $view;
};

//// Error Custom
//$container['phpErrorHandler'] = function ($c) {
//    return function ($request, $response, $error) use ($c) {
//        return $c['view']->render($response, 'view/public/500.twig')->withStatus(500);
//
//    };
//};
//
//$container['errorHandler'] = function ($c) {
//    return function ($request, $response, $error) use ($c) {
//        return $c['view']->render($response, 'view/public/500.twig')->withStatus(500);
//
//    };
//};
//
////Override the default Not Found Handler
//$container['notFoundHandler'] = function ($c) {
//    return function ($request, $response) use ($c) {
//        return $c['view']->render($response, 'view/public/404.twig')->withStatus(404);
//    };
//};
//
//$container['notAllowedHandler'] = function ($c) {
//    return function ($request, $response, $methods) use ($c) {
//        return $c['view']->render($response, 'view/public/405.twig')->withStatus(405);
//
//    };
//};
