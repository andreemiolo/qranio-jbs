<?php

namespace App\Http\Controller;

class BaseController {

    /**
     * @var Slim\Container
     */
    protected $container;

    public function __construct($container) {
        // make the container available in the class
        $this->container = $container;
    }
    
    /**
     * Retorna um parâmetro da requisição atual
     * @param string $name Nome do parâmetro
     * @param mixed $default Valor padrão caso o parâmetro não exista
     * @param bool $filter Aplicar filtro anti XSS?
     * @return mixed
     */
    protected function getParam($name, $default = null, $filter = true) {
        $param = $this->container['request']->getParam($name, $default);
        return $filter ? $this->container['purifier']->purify($param) : $param;

    }
    

    /**
     * Renderiza uma view
     * @param \Slim\Http\Response $response 
     * @param string $name Arquivo de view a ser renderizado
     * @param array $vars Variáveis a serem passadas para a view
     * @return \Slim\Http\Response
     */
    protected function view($response, $name, array $vars = []) {
        return $this->container->view->render($response, $name, $vars);
    }

    /**
     * Envia uma resposta do tipo HTML
     * @param mixed $content Conteúdo da resposta
     * @param int $status Código de status da resposta
     * @return \Slim\Http\Response
     */
    protected function html($content = null, $status = 200) {
        $head = new \Slim\Http\Headers();
        $head->set('Content-Type', 'text/html; charset=utf-8');
        $r = new \Slim\Http\Response($status, $head);
        $r->getBody()->write($content);
        return $r;       
    }

}
