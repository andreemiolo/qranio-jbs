<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controller;

/**
 * Description of TaskController
 *
 * @author gianmenezes
 */
class TaskController extends BaseController {

    private function getIndexArgs(array $args = []) {
        $args['toolbar_buttons'] = [[
            'route' => 'task.new',
            'class' => 'btn btn-primary',
            'icon' => 'fa fa-plus-circle',
            'name' => 'BTN_ADD',
        ]];
        $args['table_buttons'] = [[
            'route' => 'task.view',
            'route_params' => ['id_task' => "id_replace_url1"],
            'id_replace_url' => ['id_task'],
            'button_class' => 'edit',
            'button_icon' => 'fa fa-eye',
            'button_name' => 'BTN_VIEW',
        ]];
        return $args;
    }

    private function getViewTaskView($response, $idTask, array $args = [], $overrideExecutor = true, $overrideDates = true) {
        $args['floating_buttons'] = [];
        $data_ws = [
            'id_task' => $idTask,
        ];
        $service = new \App\Http\Service\TaskService($this->container);
        $wsData = $service->getTask($data_ws);
        if ($wsData['error'] === true) {
            $vars = [
                'alert_error' => $wsData['localized_messages']
            ];
            $vars = $this->getIndexArgs($vars);
            return $this->view($response, 'view/task/index.twig', $vars);
        }
        $args['task'] = $wsData['data'];
        if ($args['task']['task_status'] != 'A') {
            array_push($args['floating_buttons'], [
                'route' => '',
                'route_params' => [],
                'click_event' => "showForm('reopen_task')",
                'button_icon' => 'fa-reply',
                'button_name' => 'BTN_REOPEN_TASK',
            ]);
        }
        if ($args['task']['id_operator'] == $this->container['session']->get('user_id') && $args['task']['task_status'] != 'E') {
            array_push($args['floating_buttons'], [
                'route' => 'task.transfer',
                'route_params' => [],
                'click_event' => "showForm('transfer_task')",
                'button_icon' => 'fa-exchange',
                'button_name' => 'BTN_TRANSFER_TASK',
            ]);
            array_push($args['floating_buttons'], [
                'route' => 'task.finish',
                'route_params' => [],
                'click_event' => "showForm('end_task')",
                'button_icon' => 'fa-handshake-o',
                'button_name' => 'BTN_END_TASK',
            ]);
        }
        if ($args['task']['id_executor'] == $this->container['session']->get('user_id') && $args['task']['id_operator'] != $this->container['session']->get('user_id') && $args['task']['task_status'] == 'A') {
            array_push($args['floating_buttons'],[
                'route' => 'task.finish',
                'route_params' => [],
                'click_event' => "showForm('wait_task')",
                'button_icon' => 'fa-handshake-o',
                'button_name' => 'BTN_WAIT_TASK',
            ]);
            if ($args['form_type'] && $args['form_type'] == 'end_task') {
                $args['form_type'] = 'wait_task';
            }
        }
        array_push($args['floating_buttons'],[
            'route' => 'task.update',
            'route_params' => [],
            'click_event' => "showForm('edit_task')",
            'button_icon' => 'fa-edit',
            'button_name' => 'BTN_EDIT_TASK',
        ]);
        array_push($args['floating_buttons'],[
            'route' => 'task.progress.insert',
            'route_params' => [],
            'click_event' => "showForm('add_task_progress')",
            'button_icon' => 'fa-plus',
            'button_name' => 'BTN_ADD_PROGRESS',
        ]);

        if (!isset($args['form_task'])) {
            $form_task = new \App\Form\TaskForm(new \Aura\Input\Builder(), new \Aura\Input\Filter(), ['helper' => $this->container['helper']]);
            $task = $args['task'];
            $task['task_initial_date'] = $this->container['helper']->convertDate($task['task_initial_date'], 'Y-m-d H:i:s', 'd/m/Y H:i');
            $task['task_final_date'] = $this->container['helper']->convertDate($task['task_final_date'], 'Y-m-d H:i:s', 'd/m/Y H:i');
            $form_task->fill($task);
            $args['form_task'] = $form_task;
        }
        if (isset($args['form_progress'])) {
            $args['form_progress']->fill(['id_current_executor' => $args['task']['id_executor']]);
            if ($overrideExecutor) {
                $args['form_progress']->fill(['id_executor' => $args['task']['id_executor']]);
            }
            if ($overrideDates) {
                $args['form_progress']->fill(['task_reopen_initial_date' => $this->container['helper']->convertDate(date('Y-m-d H:i:s'), 'Y-m-d H:i:s', 'd/m/Y H:i')]);
                $args['form_progress']->fill(['task_reopen_final_date' => $this->container['helper']->convertDate(date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . ' +1 day')), 'Y-m-d H:i:s', 'd/m/Y H:i')]);
            }
        }
        return $this->view($response, 'view/task/view.twig', $args);
    }

    public function taskIndex($request, $response) {
        $args = $this->getIndexArgs();
        return $this->view($response, 'view/task/index.twig', $args);
        //$this->view($response, 'view/task/general.twig');
    }

    public function taskList($request, $response) {
        //Instancia o serviço de Administrator para o WS
        $service = new \App\Http\Service\TaskService($this->container);

        $data_ws = [];

        //Pega todos os parametros para atualizar a consulta
        $offset = $this->getParam('offset', 0);
        $limit = $this->getParam('limit', 20);
        $search = $this->getParam('search', '');
        $sort = $this->getParam('sort', 'task_priority');
        $order = $this->getParam('order', 'asc');

        $data_ws = [
            'offset' => $offset,
            'limit' => $limit,
            'order_list' => $sort . ' ' . $order,
        ];
        if ($search) {
            $data_ws['filter'] = 'filtro_geral:' . $search;
        }

        try {
            //Faz o envio e pega a resposta no WS
            $wsData = $service->listTasks($data_ws);

            //Retorna pra view os dados
            $json = json_encode($wsData['data']);
            return $json;
        }
        catch (\Exception $e) {
            syslog(LOG_ERR, $e->getMessage());
        }
    }

    /**
     * @param $request
     * @param $response
     * @param $args
     * @throws \Exception
     * @return \Slim\Http\Response
     */
    public function taskView($request, $response) {
        $id_task = $request->getAttribute('id_task');
        $form_progress = new \App\Form\TaskProgressForm(new \Aura\Input\Builder(), new \Aura\Input\Filter(), ['helper' => $this->container['helper']]);
        $form_progress->fill(['id_task' => $id_task]);
        $args['form_progress'] = $form_progress;
        return $this->getViewTaskView($response, $id_task, $args);
    }



    /**
     * @param $request
     * @param $response
     * @return \Slim\Http\Response
     */
    public function taskNew($request, $response) {
        $form = new \App\Form\TaskInsertForm(new \Aura\Input\Builder(), new \Aura\Input\Filter(), ['helper' => $this->container['helper']]);
        $form->init();
        $args = [
            'form' => $form
        ];
        return $this->view($response, 'view/task/new.twig', $args);
    }

    /**
     * @param $request
     * @param $response
     * @param $args
     * @throws \Exception
     * @return \Slim\Http\Response
     */
    public function taskInsert($request, $response) {
        if (false === $request->getAttribute('csrf_status')) {
            $msg_error = [
                'number' => 00214,
                'title' => $this->container['helper']->qrTranslate('VALIDATOR_ERROR_CSRF_TITLE'),
                'msg' => $this->container['helper']->qrTranslate('VALIDATOR_ERROR_CSRF_MESSAGE')
            ];

            $url = $this->container['router']->pathFor('error', [], $msg_error);
            return $response->withStatus(404)->withHeader('Location', $url);
        }
        //Pega os Parametros enviado pelo request e compacta em $data
        $id_executor = $this->getParam('id_executor');
        $name = $this->getParam('name');
        $description = $this->getParam('description');
        $task_initial_date = $this->getParam('task_initial_date');
        $task_final_date = $this->getParam('task_final_date');
        $id_operator = $this->container['session']->get('user_id');
        $data = compact('id_executor', 'name', 'description', 'task_initial_date', 'task_final_date', 'id_operator');

        //Cria o Form Aura do Controller pra validar os campos enviados
        $form = new \App\Form\TaskInsertForm(new \Aura\Input\Builder(), new \Aura\Input\Filter(), ['helper' => $this->container['helper']]);
        //Pega os parametros do request para passar no filtro
        $form->fill($data);
        //Se nao for válido os parametros enviado é exibido o erro no formulário
        if (!$form->filter()) {
            return $this->view($response, 'view/task/new.twig', ['form' => $form]);
        }
        
        $service = new \App\Http\Service\TaskService($this->container);
        //Converto a data para o formato do WS
        $data['task_initial_date'] = $this->container['helper']->convertDate($data['task_initial_date'], 'd/m/Y H:i', 'Y-m-d H:i:00');
        $data['task_final_date'] = $this->container['helper']->convertDate($data['task_final_date'], 'd/m/Y H:i', 'Y-m-d H:i:00');

        $wsData = $service->insertTask($data);

        if (!is_array($wsData)) {
            return;
        }
        if ($wsData['error'] === true) {
            $vars = [
                'form' => $form,
                'alert_error' => $wsData['localized_messages']
            ];

            return $this->view($response, 'view/task/new.twig', $vars);
        }

        // Limpa o Formulário
        $form->init();

        $vars = [
            'form' => $form,
            'alert_success' => [qrTranslate('INSERT_SUCCESS')]
        ];

        return $this->view($response, 'view/task/new.twig', $vars);
    }

    /**
     * @param $request
     * @param $response
     * @param $args
     * @throws \Exception
     * @return \Slim\Http\Response
     */
    public function taskUpdate($request, $response) {
        if (false === $request->getAttribute('csrf_status')) {
            $msg_error = [
                'number' => 00214,
                'title' => $this->container['helper']->qrTranslate('VALIDATOR_ERROR_CSRF_TITLE'),
                'msg' => $this->container['helper']->qrTranslate('VALIDATOR_ERROR_CSRF_MESSAGE')
            ];

            $url = $this->container['router']->pathFor('error', [], $msg_error);
            return $response->withStatus(404)->withHeader('Location', $url);
        }
        //Pega os Parametros enviado pelo request e compacta em $data
        $id_task = $this->getParam('id_task');
        $id_operator = $this->getParam('id_operator');
        $name = $this->getParam('name');
        $description = $this->getParam('description');
        $task_initial_date = $this->getParam('task_initial_date');
        $task_final_date = $this->getParam('task_final_date');
        $data = compact('id_task', 'id_operator', 'name', 'description', 'task_initial_date', 'task_final_date', 'id_operator');

        //Cria o Form Aura do Controller pra validar os campos enviados
        $form_task = new \App\Form\TaskForm(new \Aura\Input\Builder(), new \Aura\Input\Filter(), ['helper' => $this->container['helper']]);
        $form_progress = new \App\Form\TaskProgressForm(new \Aura\Input\Builder(), new \Aura\Input\Filter(), ['helper' => $this->container['helper']]);
        $form_progress->fill(['id_task' => $id_task]);
        //Pega os parametros do request para passar no filtro
        $form_task->fill($data);
        //Se nao for válido os parametros enviado é exibido o erro no formulário
        if (!$form_task->filter()) {
            $vars = [
                'form_progress' => $form_progress,
                'form_task' => $form_task,
                'form_type' => 'edit_task',
            ];
            return $this->getViewTaskView($response, $id_task, $vars);
        }

        //Converto a data para o formato do WS
        $data['task_initial_date'] = $this->container['helper']->convertDate($data['task_initial_date'], 'd/m/Y H:i', 'Y-m-d H:i:00');
        $data['task_final_date'] = $this->container['helper']->convertDate($data['task_final_date'], 'd/m/Y H:i', 'Y-m-d H:i:00');

        $service = new \App\Http\Service\TaskService($this->container);
        $wsData = $service->updateTask($data);
        if (!is_array($wsData)) {
            return;
        }
        if ($wsData['error'] === true) {
            $vars = [
                'form_progress' => $form_progress,
                'form_task' => $form_task,
                'form_type' => 'edit_task',
                'alert_error' => $wsData['localized_messages'],
            ];
            return $this->getViewTaskView($response, $id_task, $vars);
        }
        $vars = [
            'form_progress' => $form_progress,
            'form_task' => $form_task,
            'alert_success' => [qrTranslate('UPDATE_SUCCESS')]
        ];
        return $this->getViewTaskView($response, $id_task, $vars);
    }

    /**
     * @param $request
     * @param $response
     * @param $args
     * @throws \Exception
     * @return \Slim\Http\Response
     */
    public function taskProgressInsert($request, $response) {
        if (false === $request->getAttribute('csrf_status')) {
            $msg_error = [
                'number' => 00214,
                'title' => $this->container['helper']->qrTranslate('VALIDATOR_ERROR_CSRF_TITLE'),
                'msg' => $this->container['helper']->qrTranslate('VALIDATOR_ERROR_CSRF_MESSAGE')
            ];

            $url = $this->container['router']->pathFor('error', [], $msg_error);
            return $response->withStatus(404)->withHeader('Location', $url);
        }
        //Pega os Parametros enviado pelo request e compacta em $data
        $id_task = $this->getParam('id_task');
        $task_progress = $this->getParam('task_progress');
        $id_user = $this->container['session']->get('user_id');
        $data = compact('id_user', 'task_progress', 'id_task');

        //Cria o Form Aura do Controller pra validar os campos enviados
        $form_progress = new \App\Form\TaskProgressForm(new \Aura\Input\Builder(), new \Aura\Input\Filter(), ['helper' => $this->container['helper']]);
        //Pega os parametros do request para passar no filtro
        $form_progress->fill($data);
        //Se nao for válido os parametros enviado é exibido o erro no formulário
        if (!$form_progress->filter()) {
            $vars = ['form_progress' => $form_progress];
            $vars['form_type'] = 'add_task_progress';
            return $this->getViewTaskView($response, $id_task, $vars);
        }

        $service = new \App\Http\Service\TaskService($this->container);
        $wsData = $service->insertTaskProgress($data);

        if (!is_array($wsData)) {
            return;
        }
        if ($wsData['error'] === true) {
            $vars = [
                'form_progress' => $form_progress,
                'alert_error' => $wsData['localized_messages'],
                'form_type' => 'add_task_progress',
            ];
            return $this->getViewTaskView($response, $id_task, $vars);
        }

        // Limpa o Formulário
        $form_progress->init();
        $form_progress->fill(['id_task' => $id_task]);

        $vars = [
            'form_progress' => $form_progress,
            'alert_success' => [qrTranslate('INSERT_SUCCESS')]
        ];
        return $this->getViewTaskView($response, $id_task, $vars);
    }

    /**
     * @param $request
     * @param $response
     * @param $args
     * @throws \Exception
     * @return \Slim\Http\Response
     */
    public function taskTransfer($request, $response) {
        if (false === $request->getAttribute('csrf_status')) {
            $msg_error = [
                'number' => 00214,
                'title' => $this->container['helper']->qrTranslate('VALIDATOR_ERROR_CSRF_TITLE'),
                'msg' => $this->container['helper']->qrTranslate('VALIDATOR_ERROR_CSRF_MESSAGE')
            ];

            $url = $this->container['router']->pathFor('error', [], $msg_error);
            return $response->withStatus(404)->withHeader('Location', $url);
        }
        //Pega os Parametros enviado pelo request e compacta em $data
        $id_task = $this->getParam('id_task');
        $id_executor = $this->getParam('id_executor');
        $id_current_executor = $this->getParam('id_current_executor');
        $task_progress = $this->getParam('task_progress');
        $id_user = $this->container['session']->get('user_id');
        $data = compact('id_user', 'task_progress', 'id_task', 'id_executor', 'id_current_executor');

        //Cria o Form Aura do Controller pra validar os campos enviados
        $form_progress = new \App\Form\TaskTransferForm(new \Aura\Input\Builder(), new \Aura\Input\Filter(), ['helper' => $this->container['helper']]);
        //Pega os parametros do request para passar no filtro
        $form_progress->fill($data);
        //Se nao for válido os parametros enviado é exibido o erro no formulário
        if (!$form_progress->filter()) {
            $vars = ['form_progress' => $form_progress];
            $vars['form_type'] = 'transfer_task';
            return $this->getViewTaskView($response, $id_task, $vars, false);
        }

        $service = new \App\Http\Service\TaskService($this->container);
        $wsData = $service->transferTask($data);

        if (!is_array($wsData)) {
            return;
        }
        if ($wsData['error'] === true) {
            $vars = [
                'form_progress' => $form_progress,
                'alert_error' => $wsData['localized_messages'],
                'form_type' => 'transfer_task',
            ];
            return $this->getViewTaskView($response, $id_task, $vars, false);
        }

        // Limpa o Formulário
        $form_progress->init();
        $form_progress->fill(['id_task' => $id_task]);

        $vars = [
            'form_progress' => $form_progress,
            'alert_success' => [qrTranslate('INSERT_SUCCESS')]
        ];
        return $this->getViewTaskView($response, $id_task, $vars);
    }

    /**
     * @param $request
     * @param $response
     * @param $args
     * @throws \Exception
     * @return \Slim\Http\Response
     */
    public function taskFinish($request, $response) {
        if (false === $request->getAttribute('csrf_status')) {
            $msg_error = [
                'number' => 00214,
                'title' => $this->container['helper']->qrTranslate('VALIDATOR_ERROR_CSRF_TITLE'),
                'msg' => $this->container['helper']->qrTranslate('VALIDATOR_ERROR_CSRF_MESSAGE')
            ];

            $url = $this->container['router']->pathFor('error', [], $msg_error);
            return $response->withStatus(404)->withHeader('Location', $url);
        }
        //Pega os Parametros enviado pelo request e compacta em $data
        $id_task = $this->getParam('id_task');
        $task_progress = $this->getParam('task_progress');
        $id_user = $this->container['session']->get('user_id');
        $data = compact('id_user', 'task_progress', 'id_task');

        //Cria o Form Aura do Controller pra validar os campos enviados
        $form_progress = new \App\Form\TaskProgressForm(new \Aura\Input\Builder(), new \Aura\Input\Filter(), ['helper' => $this->container['helper']]);
        //Pega os parametros do request para passar no filtro
        $form_progress->fill($data);
        //Se nao for válido os parametros enviado é exibido o erro no formulário
        if (!$form_progress->filter()) {
            $vars = ['form_progress' => $form_progress];
            $vars['form_type'] = 'end_task';
            return $this->getViewTaskView($response, $id_task, $vars, false);
        }

        $service = new \App\Http\Service\TaskService($this->container);
        $wsData = $service->finishTask($data);

        if (!is_array($wsData)) {
            return;
        }
        if ($wsData['error'] === true) {
            $vars = [
                'form_progress' => $form_progress,
                'alert_error' => $wsData['localized_messages'],
                'form_type' => 'end_task',
            ];
            return $this->getViewTaskView($response, $id_task, $vars, false);
        }

        // Limpa o Formulário
        $form_progress->init();
        $form_progress->fill(['id_task' => $id_task]);

        $vars = [
            'form_progress' => $form_progress,
            'alert_success' => [qrTranslate('INSERT_SUCCESS')]
        ];
        return $this->getViewTaskView($response, $id_task, $vars);
    }

    /**
     * @param $request
     * @param $response
     * @param $args
     * @throws \Exception
     * @return \Slim\Http\Response
     */
    public function taskReopen($request, $response) {
        if (false === $request->getAttribute('csrf_status')) {
            $msg_error = [
                'number' => 00214,
                'title' => $this->container['helper']->qrTranslate('VALIDATOR_ERROR_CSRF_TITLE'),
                'msg' => $this->container['helper']->qrTranslate('VALIDATOR_ERROR_CSRF_MESSAGE')
            ];

            $url = $this->container['router']->pathFor('error', [], $msg_error);
            return $response->withStatus(404)->withHeader('Location', $url);
        }
        //Pega os Parametros enviado pelo request e compacta em $data
        $id_task = $this->getParam('id_task');
        $id_executor = $this->getParam('id_executor');
        $id_current_executor = $this->getParam('id_current_executor');
        $task_progress = $this->getParam('task_progress');
        $id_user = $this->container['session']->get('user_id');
        $task_reopen_initial_date = $this->getParam('task_reopen_initial_date');
        $task_reopen_final_date = $this->getParam('task_reopen_final_date');
        $data = compact('id_user', 'task_progress', 'id_task', 'id_executor', 'id_current_executor', 'task_reopen_initial_date', 'task_reopen_final_date');

        //Cria o Form Aura do Controller pra validar os campos enviados
        $form_progress = new \App\Form\TaskReopenForm(new \Aura\Input\Builder(), new \Aura\Input\Filter(), ['helper' => $this->container['helper']]);
        //Pega os parametros do request para passar no filtro
        $form_progress->fill($data);
        //Se nao for válido os parametros enviado é exibido o erro no formulário
        if (!$form_progress->filter()) {
            $vars = ['form_progress' => $form_progress];
            $vars['form_type'] = 'reopen_task';
            return $this->getViewTaskView($response, $id_task, $vars, false, false);
        }
        $data['task_initial_date'] = $this->container['helper']->convertDate($data['task_reopen_initial_date'], 'd/m/Y H:i', 'Y-m-d H:i:00');
        $data['task_final_date'] = $this->container['helper']->convertDate($data['task_reopen_final_date'], 'd/m/Y H:i', 'Y-m-d H:i:00');
        $service = new \App\Http\Service\TaskService($this->container);
        $wsData = $service->reopenTask($data);

        if (!is_array($wsData)) {
            return;
        }
        if ($wsData['error'] === true) {
            syslog(LOG_ERR, 'Erro no retorno do WS');
            $vars = [
                'form_progress' => $form_progress,
                'alert_error' => $wsData['localized_messages'],
                'form_type' => 'reopen_task',
            ];
            return $this->getViewTaskView($response, $id_task, $vars, false, false);
        }

        // Limpa o Formulário
        $form_progress->init();
        $form_progress->fill(['id_task' => $id_task]);

        $vars = [
            'form_progress' => $form_progress,
            'alert_success' => [qrTranslate('INSERT_SUCCESS')]
        ];
        return $this->getViewTaskView($response, $id_task, $vars);
    }

}
