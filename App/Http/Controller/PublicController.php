<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controller;

/**
 * Description of PublicController
 *
 * @author gianmenezes
 */
class PublicController extends BaseController {

    public function index($request, $response) {

        //Verifica se o usuário está logado
        $is_autenticate = $this->container['helper']->isAuthenticated();
        if ($is_autenticate) {
            $url = $this->container['router']->pathFor('home');
            return $response->withStatus(302)->withHeader('Location', $url);
        }
        $form = new \App\Form\LoginForm(new \Aura\Input\Builder, new \Aura\Input\Filter);
        $vars = [
            'form' => $form
        ];
        return $this->view($response, 'view/public/login.twig', $vars);
    }

    public function logout($request, \Slim\Http\Response $response) {
        $this->container['session']->destroy();
        $url = $this->container['router']->pathFor('index');
        return $response->withStatus(302)->withHeader('Location', $url);
    }

    public function error(\Slim\Http\Request $request, \Slim\Http\Response $response) {

        $vars = $request->getParams();

        return $this->view($response, 'view/public/error.twig', $vars);
    }

    public function recoveryPassword($request, $response) {

        $form = new \App\Form\RecoveryPasswordForm(new \Aura\Input\Builder, new \Aura\Input\Filter);

        // Verifica se a rota não é post pra que possa ser exibida a tela inicial
        if ($request->isPost() === false) {

            $vars = [
                'form' => $form
            ];
            return $this->view($response, 'view/public/user-recovery-password.twig', $vars);
        }

        //Pega os Parametros enviado pelo request e compacta em $data
        $email = $this->getParam('email');
        $data = compact('email');

        //Pega os parametros do request para passar no filtro
        $form->fill($data);

        //Se nao for válido os parametros enviado é exibido o erro no formulário
        if (!$form->filter()) {
            return $this->view($response, 'view/public/user-recovery-password.twig', ['form' => $form]);
        }

        //Instacia o serviço de Login para o WS
        $servLogin = new \App\Http\Service\PublicService($this->container);
        /* @var $servLogin \App\Http\Service\PublicService */

        $data_ws = [
            'email' => $email
        ];

        $wsData = $servLogin->recoveryPassword($data_ws);

        if (!is_array($wsData)) {
            return;
        }

//        var_dump($wsData);
//        die();

        if ($wsData['error'] === true) {

            $vars = [
                'form'        => $form,
                'alert_error' => $wsData['localized_messages']
            ];

            return $this->view($response, 'view/public/user-recovery-password.twig', $vars);
        }

        $vars = [
            'form'        => $form,
            'alert_error' => $wsData['localized_messages']
        ];

        return $this->view($response, 'view/public/user-recovery-password.twig', $vars);
    }

    public function confirmAccount($request, $response) {
        $this->view($response, 'view/public/user-confirm-account.twig');
    }

    public function newPassword($request, $response) {
        $this->view($response, 'view/public/user-new-password.twig');
    }

    public function asset(\Slim\Http\Request $request, \Slim\Http\Response $response) {
        //Retorna erro 404 caso o arquivo não for encontrado
        return $this->view($response, 'view/public/404.twig')->withStatus(404);
    }

}
