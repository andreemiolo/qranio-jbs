<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controller;

/**
 * Description of HomeController
 *
 * @author gianmenezes
 */


class HomeController extends BaseController
{
    
    
    public function home($request, $response, $args)
    {
        $this->view($response, 'view/home/home.twig', $args);
    }
}

