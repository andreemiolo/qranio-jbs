<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controller;

/**
 * Description of HomeController
 *
 * @author gianmenezes
 */


class AccountController extends BaseController
{
    
    
    public function profile($request, $response, $args)
    {   
        $this->view($response, 'view/account/profile.twig', $args);
    }
    
    public function profileEdit($request, $response, $args)
    {   
        $this->view($response, 'view/account/profile-edit.twig', $args);
    }
}

