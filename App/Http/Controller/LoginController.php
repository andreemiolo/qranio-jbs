<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controller;
use Aura\Input\Exception\CsrfViolation;

/**
 * Description of LoginController
 *
 * @author gianmenezes
 */
class LoginController extends BaseController {

    public function loginApp(\Slim\Http\Request $request, \Slim\Http\Response $response) {
//        //Cria a Sessão com os dados do usuário
//        $this->container['session']->set('cerebelo', ['user_profile' => $wsData['data']['user']]);
//        $this->container['session']->set('authqr', true);
//
//        return $response->withStatus(302)->withHeader('Location', 'index');

        //Pega os Parametros enviado pelo request e compacta em $data
        $email = $this->getParam('email');
        $password = $this->getParam('password');
        $next_url = $this->getParam('next_url');
        $data = compact('email', 'password');

        //Cria o Form Aura do Controller pra validar os campos enviados
        $form = new \App\Form\LoginForm(new \Aura\Input\Builder, new \Aura\Input\Filter);

        //Pega os parametros do request para passar no filtro
        $form->fill($data);

        //Se nao for válido os parametros enviado é exibido o erro no formulário
        if (!$form->filter()) {
            return $this->view($response, 'view/public/login.twig', ['form' => $form]);
        }

        //Instacia o serviço de Login para o WS
        $servLogin = new \App\Http\Service\LoginService($this->container);
        /* @var $servLogin \App\Http\Service\LoginService */

        $data_ws = [
            'email'    => $email,
            'password' => $this->container['helper']->qrEcryptPassword($password)
        ];

        $wsData = $servLogin->login($data_ws);

        if (!is_array($wsData)) {
            return;
        }

        if ($wsData['error'] === true) {
            
            $vars = [
                'form'        => $form,
                'alert_error' => $wsData['localized_messages']
            ];

            return $this->view($response, 'view/public/login.twig', $vars);
        }

        $menus = $wsData['data']['menus'];
        if (QR_ENV === 'development') {
            $menus = $this->container['helper']->removeInvalidRoutesFromMenu($menus);
        }

        //Cria a Sessão com os dados do usuário
        $this->container['session']->set('cerebelo', ['user_profile' => $wsData['data']['user']]);
        $this->container['session']->set('authqr', true);
        $this->container['session']->set('user_id', $wsData['data']['user']['id']);
        $this->container['session']->set('user_language', $wsData['data']['user']['language']);
        $this->container['session']->set('menu', $menus);
        $this->container['session']->set('route_allowed', $wsData['data']['available_routes']);
        $this->container['session']->set('full_access', $wsData['data']['user']['full_access']);
        $this->container['session']->set('expired_password', $wsData['data']['user']['expired_password']);
        $this->container['session']->set('temporary_password', $wsData['data']['user']['temporary_password']);

        return $response->withStatus(302)->withHeader('Location', 'index');
    }

    /**
     * Abre a tela para trocar a senha temporária do usuário
     * @param \Slim\Http\Request $request
     * @param \Slim\Http\Response $response
     * @throws \Aura\Input\Exception\CsrfViolation
     * @return \Slim\Http\Response
     */
    public function temporaryPassword($request, $response){
        $id_user = $this->container['session']->get('user_id');

        $serv = new \App\Http\Service\AdministratorService($this->container);

        $wsData = $serv->getUser(['id_user' => $id_user]);

        if ($wsData['data']['has_temporary_password'] !== 'Y') {
            $this->container['session']->set('temporary_password', $wsData['data']['has_temporary_password']);
            //se não tem senha temporária, não posso recuperar senha
            return $response->withStatus(302)->withHeader('Location', 'index');
        }
        $form = new \App\Form\RedefinePasswordForm(new \Aura\Input\Builder, new \Aura\Input\Filter);

        $form->fill(['id_user' => $id_user, 'expired' => 'N']);

        $vars = [
            'expired' => false,
            'form' => $form,
        ];

        return $this->view($response, 'view/public/redefine-password.twig', $vars);
    }

    /**
     * Abre a tela para trocar a senha expirada do usuário
     * @param \Slim\Http\Request $request
     * @param \Slim\Http\Response $response
     * @throws \Aura\Input\Exception\CsrfViolation
     * @return \Slim\Http\Response
     */
    public function expiredPassword($request, $response){
        $id_user = $this->container['session']->get('user_id');

        $serv = new \App\Http\Service\AdministratorService($this->container);

        $wsData = $serv->getUser(['id_user' => $id_user]);

        if ($wsData['data']['password_expired'] !== 'Y') {
            $this->container['session']->set('expired_password', $wsData['data']['password_expired']);
            //se não tem senha expirada, não posso redefinir senha
            return $response->withStatus(302)->withHeader('Location', 'index');
        }
        $form = new \App\Form\RedefinePasswordForm(new \Aura\Input\Builder, new \Aura\Input\Filter);

        $form->fill(['id_user' => $id_user, 'expired' => 'Y']);

        $vars = [
            'expired' => true,
            'form' => $form,
        ];

        return $this->view($response, 'view/public/redefine-password.twig', $vars);
    }

    /**
     * Redefine a senha do usuário
     * @param \Slim\Http\Request $request
     * @param \Slim\Http\Response $response
     * @throws \Aura\Input\Exception\CsrfViolation
     * @return \Slim\Http\Response
     */
    public function redefinePassword($request, $response){
        $id_user = $this->getParam('id_user');
        $expired = $this->getParam('expired');
        $password = $this->getParam('password');
        $confirm_password = $this->getParam('confirm_password');
        $data = compact('id_user', 'expired', 'password', 'confirm_password');

        //Cria o Form Aura do Controller pra validar os campos enviados
        $form = new \App\Form\RedefinePasswordForm(new \Aura\Input\Builder(), new \Aura\Input\Filter(), ['helper' => $this->container['helper']]);

        //Pega os parametros do request para passar no filtro
        $form->fill($data);

        //Se nao for válido os parametros enviado é exibido o erro no formulário
        if (!$form->filter()) {
            return $this->view($response, 'view/public/redefine-password.twig', ['form' => $form, 'expired' => ($expired == 'Y'),]);
        }

        $serv = new \App\Http\Service\LoginService($this->container);

        $data = [
            'id_user'  => $id_user,
            'password' => hash('sha256', md5($password))
        ];

        $wsData = $serv->updatePassword($data);

        if (!is_array($wsData)) {
            return;
        }

        if ($wsData['error'] === true) {

            $vars = [
                'form' => $form,
                'expired' => ($expired == 'Y'),
                'alert_error' => $wsData['localized_messages']
            ];

            return $this->view($response, 'view/public/redefine-password.twig', $vars);
        }
        $field = ($expired == 'Y') ? 'expired_password' : 'temporary_password';
        $this->container['session']->set($field, 'N');

        return $response->withStatus(302)->withHeader('Location', 'index');

    }

}
