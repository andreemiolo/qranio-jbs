<?php
/**
 * Created by PhpStorm.
 * User: gianmenezes
 * Date: 25/09/17
 * Time: 19:47
 */

namespace App\Http\Controller;


class UserController extends BaseController
{

    /**
     * @param $request
     * @param $response
     * @param $args
     * @return Slim\Http\Response|void
     */
    public function userList($request, $response, $args)
    {
        return $this->view($response, 'view/user/list.twig', $args);
    }


    /**
     * @param $request
     * @param $response
     * @return string
     */
    public function userListTable($request, $response, $args)
    {
        //Instacia o serviço de Login para o WS
        $serviceUser = new \App\Http\Service\UserService($this->container);
        /* @var $serviceUser \App\Http\Service\UserService */

        $data_ws = [];
        $data_ws['offset'] = '0';

        //Pega todos os parametros para atualizar a consulta
        $params = $request->getParams();

        if($params) {
            foreach ($params as $key => $value) {
                //Verifica se está vazio se estiver não adiciona no array para fazer a requisição
                if (!empty($value)) {
                    if($key == 'order'){
                        $data_ws['order_type'] = $this->getParam($key);
                    }else{
                        $data_ws[$key] = $this->getParam($key);
                    }

                }
            }
        }

        //Faz o envio e pega a resposta no WS
        $wsData = $serviceUser->listUserApp($data_ws);

        //var_dump($wsData['data']);

        if(empty($wsData['data'])){
            //TODO - ALterar no WS pra que quando nao tiver resultado manter o array
            $wsData['data'] = [
                'users'        => []
            ];

        }

        return json_encode($wsData['data']);

    }

}