<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controller;

/**
 * Description of AdministratorController
 *
 * @author gianmenezes
 */
class AdministratorController extends BaseController
{

//<editor-fold desc="GRUPOS">
    /**
     * @param $request
     * @param $response
     * @param $args
     * @return \Slim\Http\Response
     */
    public function groupPermission($request, $response, $args) {
        //Instancia o serviço de Administrator para o WS
        $service= new \App\Http\Service\AdministratorService($this->container);
        $id_group = $request->getAttribute('id_group');

        $data_ws = $service->listPermissions(['id_group' => $id_group])['data'];
        $args['actions'] = $data_ws['actions'];
        $args['permissions'] = $data_ws['permissions'];
        $args['actions_json'] = json_encode($data_ws['actions']);
        $args['permissions_json'] = json_encode($data_ws['permissions']);
        $args['id_group'] = $id_group;

        /* @var $serviceGroup \App\Http\Service\AdministratorService */
        return $this->view($response, 'view/administrator/group/permission.twig', $args);
    }

    /**
     * @param $request
     * @param $response
     * @param $args
     * @return \Slim\Http\Response
     */
    public function groupPermissionUpdate($request, $response) {
        //Instacia o serviço de Login para o WS
        $service = new \App\Http\Service\AdministratorService($this->container);
        /* @var $service \App\Http\Service\AdministratorService */

        if (false === $request->getAttribute('csrf_status')) {

            $msg_error = [
                'number' => 00214,
                'title' => $this->container['helper']->qrTranslate('VALIDATOR_ERROR_CSRF_TITLE'),
                'msg' => $this->container['helper']->qrTranslate('VALIDATOR_ERROR_CSRF_MESSAGE')
            ];

            $url = $this->container['router']->pathFor('error', [], $msg_error);
            return $response->withStatus(404)->withHeader('Location', $url);

        }

        $id_group = $this->getParam('id_group');
        $actions = json_decode($this->getParam('actions'), true);
        $permissions = json_decode($this->getParam('permissions'), true);
        $form_permissions = $this->getParam('form_permissions', [], false);

        foreach ($permissions as $key => &$value) {
            $value = in_array((string) $key, $form_permissions) ? 'Y' : 'N';
        }

        $data = [
            'actions' => $actions,
            'permissions' => $permissions,
            'actions_json' => json_encode($actions),
            'permissions_json' => json_encode($permissions),
            'id_group' => $id_group
        ];

        $service = new \App\Http\Service\AdministratorService($this->container);

        $wsData = $service->updatePermissions([
            'id_group' => $id_group,
            'actions_allowed' => implode(',',$form_permissions)
        ]);

        if (!is_array($wsData)) {
            return;
        }

        if ($wsData['error'] === true) {
            $data['alert_error'] = $wsData['localized_messages'];
        }
        else {
            $data['alert_success'] = [qrTranslate('UPDATE_SUCCESS')];
        }

        return $this->view($response, 'view/administrator/group/permission.twig', $data);

    }

    /**
     * Método para gerar os argumentos necessários para configurar o index corretamente
     * @param array $args argumentos já existentes
     * @return array retorna os argumentos corretamente configurados
     */
    private function getGroupIndexArgs(array $args = []) {
        $args['toolbar_buttons'] = [[
            'route' => 'administrator.group.new',
            'class' => 'btn btn-primary',
            'icon' => 'fa fa-plus-circle',
            'name' => 'BTN_ADD',
        ]];
        $args['table_buttons'] = [[
            'route' => 'administrator.group.edit',
            'route_params' => ['id_group' => "id_replace_url1"],
            'id_replace_url' => ['id_group'],
            'exceptions' => [['field' => 'id_group', 'value' => '0']],
            'button_class' => 'edit',
            'button_icon' => 'fa fa-edit',
            'button_name' => 'BTN_EDIT',
//        ], [
//            'route' => 'administrator.group.view',
//            'route_params' => ['id_group' => "id_replace_url1"],
//            'id_replace_url' => ['id_group'],
//            'button_class' => 'view',
//            'button_icon' => 'fa fa-eye',
//            'button_name' => 'BTN_VIEW',
        ], [
            'route' => 'administrator.group.permission',
            'route_params' => ['id_group' => "id_replace_url1"],
            'id_replace_url' => ['id_group'],
            'exceptions' => [['field' => 'id_group', 'value' => '0']],
            'button_class' => 'edit',
            'button_icon' => 'fa fa-sitemap',
            'button_name' => 'BTN_PERMISSION',
        ]];
        return $args;
    }

    /**
     * @param $request
     * @param $response
     * @param $args
     * @return \Slim\Http\Response|void
     */
    public function groupIndex($request, $response, $args) {
        $args = $this->getGroupIndexArgs($args);
        return $this->view($response, 'view/administrator/group/index.twig', $args);
    }


    /**
     * @param $request
     * @param $response
     * @return string
     */
    public function groupListTable($request, $response) {
        //Instacia o serviço de Login para o WS
        $serviceGroup = new \App\Http\Service\AdministratorService($this->container);
        /* @var $serviceGroup \App\Http\Service\AdministratorService */

        $data_ws = [];

        //Pega todos os parametros para atualizar a consulta
        $params = $request->getParams();
        $offset = $this->getParam('offset', 0);
        $limit = $this->getParam('limit', 20);
        $search = $this->getParam('search', '');
        $sort = $this->getParam('sort', 'id_group');
        $order = $this->getParam('order', 'asc');

        $data_ws = [
            'offset' => $offset,
            'limit' => $limit,
            'order_list' => $sort . ' ' . $order,
        ];
        if ($search) {
            $data_ws['filter'] = 'filtro_geral:' . $search;
        }

        //Faz o envio e pega a resposta no WS
        $wsData = $serviceGroup->listGroupsCerebelo($data_ws);

        //Retorna pra view os dados
        return json_encode($wsData['data']);

    }

    /**
     * @param $request
     * @param $response
     * @param $args
     * @return \Slim\Http\Response
     */
    public function groupNew($request, $response) {


        $form = new \App\Form\AdministratorUserGroupForm(new \Aura\Input\Builder(), new \Aura\Input\Filter(), ['helper' => $this->container['helper']]);
        $form->init();

        $args = [
            'form' => $form
        ];
        return $this->view($response, 'view/administrator/group/new.twig', $args);


    }

    /**
     * @param $request
     * @param $response
     * @param $args
     * @return \Slim\Http\Response
     */
    public function groupInsert($request, $response) {
        if (false === $request->getAttribute('csrf_status')) {
            $msg_error = [
                'number' => 00214,
                'title' => $this->container['helper']->qrTranslate('VALIDATOR_ERROR_CSRF_TITLE'),
                'msg' => $this->container['helper']->qrTranslate('VALIDATOR_ERROR_CSRF_MESSAGE')
            ];

            $url = $this->container['router']->pathFor('error', [], $msg_error);
            return $response->withStatus(404)->withHeader('Location', $url);
        }
        //Pega os Parametros enviado pelo request e compacta em $data
        $title = $this->getParam('title');
        $enable = $this->getParam('enable');
        $data = compact('title', 'enable');


        //Cria o Form Aura do Controller pra validar os campos enviados
        $form = new \App\Form\AdministratorUserGroupForm(new \Aura\Input\Builder(), new \Aura\Input\Filter(), ['helper' => $this->container['helper']]);

        //Pega os parametros do request para passar no filtro
        $form->fill($data);

        //Se nao for válido os parametros enviado é exibido o erro no formulário
        if (!$form->filter()) {
            return $this->view($response, 'view/administrator/group/new.twig', ['form' => $form,]);
        }

        //Instacia o serviço de Login para o WS
        $service = new \App\Http\Service\AdministratorService($this->container);
        /* @var $service \App\Http\Service\AdministratorService */

        $wsData = $service->insertGroupCerebelo($data);


        if (!is_array($wsData)) {
            return;
        }

        if ($wsData['error'] === true) {

            $vars = [
                'form' => $form,
                'alert_error' => $wsData['localized_messages']
            ];

            return $this->view($response, 'view/administrator/group/new.twig', $vars);
        }

        // Limpa o Formulário
        $form->init();

        $vars = [
            'form' => $form,
            'alert_success' => [qrTranslate('INSERT_SUCCESS')]
        ];


        return $this->view($response, 'view/administrator/group/new.twig', $vars);


    }

    /**
     * @param $request
     * @param $response
     * @param $args
     * @return \Slim\Http\Response
     */
    public function groupEdit($request, $response) {
        // Instancia o form da tela
        $form = new \App\Form\AdministratorUserGroupForm(new \Aura\Input\Builder(), new \Aura\Input\Filter(), ['helper' => $this->container['helper']]);

        // Pega a variável id_group para buscar o registro no WS
        $id_group = $request->getAttribute('id_group');

        $data_ws = [
            'id_group' => $id_group,
        ];

        //Instacia o serviço de requisição para o WS
        $service = new \App\Http\Service\AdministratorService($this->container);
        /* @var $service \App\Http\Service\AdministratorService */
        $wsData = $service->getGroupCerebelo($data_ws);

        if ($wsData['error'] === true) {

            $vars = [
                'form' => $form,
                'alert_error' => $wsData['localized_messages']
            ];
            $vars = $this->getGroupIndexArgs($vars);

            return $this->view($response, 'view/administrator/group/index.twig', $vars);
        }

        $data = $wsData['data'];

        $form->fill($data);

        $args = [
            'form' => $form
        ];
        return $this->view($response, 'view/administrator/group/edit.twig', $args);

    }

    /**
     * @param $request
     * @param $response
     * @param $args
     * @return \Slim\Http\Response
     */
    public function groupUpdate($request, $response) {

        //Instacia o serviço de Login para o WS
        $service = new \App\Http\Service\AdministratorService($this->container);
        /* @var $service \App\Http\Service\AdministratorService */

        if (false === $request->getAttribute('csrf_status')) {

            $msg_error = [
                'number' => 00214,
                'title' => $this->container['helper']->qrTranslate('VALIDATOR_ERROR_CSRF_TITLE'),
                'msg' => $this->container['helper']->qrTranslate('VALIDATOR_ERROR_CSRF_MESSAGE')
            ];

            $url = $this->container['router']->pathFor('error', [], $msg_error);
            return $response->withStatus(404)->withHeader('Location', $url);

        }


        //Pega os Parametros enviado pelo request e compacta em $data
        $id_group = $this->getParam('id_group');
        $title = $this->getParam('title');
        $enable = $this->getParam('enable');
        $data = compact('id_group', 'title', 'enable');

        //Cria o Form Aura do Controller pra validar os campos enviados
        $form = new \App\Form\AdministratorUserGroupForm(new \Aura\Input\Builder(), new \Aura\Input\Filter(), ['helper' => $this->container['helper']]);

        //Pega os parametros do request para passar no filtro
        $form->fill($data);

        //Se nao for válido os parametros enviado é exibido o erro no formulário
        if (!$form->filter()) {
            return $this->view($response, 'view/administrator/group/edit.twig', ['form' => $form]);
        }

        //Instacia o serviço de Login para o WS
        $service = new \App\Http\Service\AdministratorService($this->container);
        /* @var $service \App\Http\Service\AdministratorService */

        $wsData = $service->updateGroupCerebelo($data);

        if (!is_array($wsData)) {
            return;
        }

        if ($wsData['error'] === true) {

            $vars = [
                'form' => $form,
                'alert_error' => $wsData['localized_messages']
            ];

            return $this->view($response, 'view/administrator/group/edit.twig', $vars);
        }

        $vars = [
            'form' => $form,
            'alert_success' => [qrTranslate('UPDATE_SUCCESS')]
        ];


        return $this->view($response, 'view/administrator/group/edit.twig', $vars);


    }

    /**
     * @param $request
     * @param $response
     * @param $args
     * @return \Slim\Http\Response
     */
    public function groupView($request, $response) {
        // Instancia o form da tela
        $form = new \App\Form\AdministratorUserGroupForm(new \Aura\Input\Builder(), new \Aura\Input\Filter(), ['helper' => $this->container['helper']]);

        // Pega a variável id_group para buscar o registro no WS
        $id_group = $request->getAttribute('id_group');

        $data_ws = [
            'id_group' => $id_group,
        ];

        //Instacia o serviço de requisição para o WS
        $service = new \App\Http\Service\AdministratorService($this->container);
        /* @var $service \App\Http\Service\AdministratorService */
        $wsData = $service->getGroupCerebelo($data_ws);

        if ($wsData['error'] === true) {

            $vars = [
                'form' => $form,
                'alert_error' => $wsData['localized_messages']
            ];

            return $this->view($response, 'view/administrator/group/list.twig', $vars);
        }

        $data = $wsData['data'];

        $form->fill($data);

        $args = [
            'form' => $form
        ];
        return $this->view($response, 'view/administrator/group/edit.twig', $args);

    }

//</editor-fold>
//<editor-fold desc="USUÁRIOS">
    /**
     * Método para gerar os argumentos necessários para configurar o index corretamente
     * @param array $args argumentos já existentes
     * @return array retorna os argumentos corretamente configurados
     */
    private function getUserIndexArgs(array $args = []) {
        $args['toolbar_buttons'] = [[
            'route' => 'administrator.user.new',
            'class' => 'btn btn-primary',
            'icon' => 'fa fa-plus-circle',
            'name' => 'BTN_ADD',
        ]];
        $args['table_buttons'] = [[
            'route' => 'administrator.user.edit',
            'route_params' => ['id_user' => "id_replace_url1"],
            'id_replace_url' => ['id_user'],
            'button_class' => 'edit',
            'button_icon' => 'fa fa-edit',
            'button_name' => 'BTN_EDIT',
//        ], [
//            'route' => 'administrator.group.view',
//            'route_params' => ['id_group' => "id_replace_url1"],
//            'id_replace_url' => ['id_group'],
//            'button_class' => 'view',
//            'button_icon' => 'fa fa-eye',
//            'button_name' => 'BTN_VIEW',
        ]];
        return $args;
    }

    /**
     * @param $request
     * @param $response
     * @param $args
     * @return \Slim\Http\Response|void
     */
    public function userIndex($request, $response, $args) {
        $args = $this->getUserIndexArgs($args);
        return $this->view($response, 'view/administrator/user/index.twig', $args);
    }

    /**
     * @param $request
     * @param $response
     * @return string
     */
    public function userList($request, $response) {
        //Instancia o serviço de Administrator para o WS
        $serviceGroup = new \App\Http\Service\AdministratorService($this->container);
        /* @var $serviceGroup \App\Http\Service\AdministratorService */

        $data_ws = [];

        //Pega todos os parametros para atualizar a consulta
        $params = $request->getParams();
        $offset = $this->getParam('offset', 0);
        $limit = $this->getParam('limit', 20);
        $search = $this->getParam('search', '');
        $sort = $this->getParam('sort', 'id_user');
        $order = $this->getParam('order', 'desc');

        if ($params) {
            $data_ws = [
                'offset' => $offset,
                'limit' => $limit,
                'order_list' => $sort . ' ' . $order,
            ];
            if ($search) {
                $data_ws['filter'] = 'filtro_geral:' . $search;
            }
        }

        try {
            //Faz o envio e pega a resposta no WS
            $wsData = $serviceGroup->listUsers($data_ws);

            //Retorna pra view os dados
            $json = json_encode($wsData['data']);
            return $json;
        }
        catch (\Exception $e) {
            syslog(LOG_ERR, $e->getMessage());
        }
    }

    /**
     * @param $request
     * @param $response
     * @param $args
     * @throws \Exception
     * @return \Slim\Http\Response
     */
    public function userEdit($request, $response) {
        // Instancia o form da tela
        $form = new \App\Form\AdministratorUserForm(new \Aura\Input\Builder(), new \Aura\Input\Filter(), ['helper' => $this->container['helper']]);

        // Pega a variável id_group para buscar o registro no WS
        $id_user = $request->getAttribute('id_user');
        $data_ws = [
            'id_user' => $id_user,
        ];
        //Instacia o serviço de requisição para o WS
        $service = new \App\Http\Service\AdministratorService($this->container);
        /* @var $service \App\Http\Service\AdministratorService */
        $wsData = $service->getUser($data_ws);

        if ($wsData['error'] === true) {
            $vars = [
                'form' => $form,
                'alert_error' => $wsData['localized_messages']
            ];
            $vars = $this->getUserIndexArgs($vars);
            return $this->view($response, 'view/administrator/user/index.twig', $vars);
        }
        $data = $wsData['data'];
        $form->fill($data);
        $args = [
            'form' => $form
        ];
        return $this->view($response, 'view/administrator/user/edit.twig', $args);
    }

    /**
     * @param $request
     * @param $response
     * @param $args
     * @throws \Exception
     * @return \Slim\Http\Response
     */
    public function userUpdate($request, $response) {
        if (false === $request->getAttribute('csrf_status')) {
            $msg_error = [
                'number' => 00214,
                'title' => $this->container['helper']->qrTranslate('VALIDATOR_ERROR_CSRF_TITLE'),
                'msg' => $this->container['helper']->qrTranslate('VALIDATOR_ERROR_CSRF_MESSAGE')
            ];

            $url = $this->container['router']->pathFor('error', [], $msg_error);
            return $response->withStatus(404)->withHeader('Location', $url);
        }
        //Pega os Parametros enviado pelo request e compacta em $data
        $id_user = $this->getParam('id_user');
        $fname = $this->getParam('fname');
        $lname = $this->getParam('lname');
        $occupation = $this->getParam('occupation');
        $email = $this->getParam('email');
        $id_group = $this->getParam('id_group');
        $enable = $this->getParam('enable');
        $language = $this->getParam('language');
        $data = compact('id_user', 'fname', 'lname', 'occupation', 'email', 'id_group', 'password', 'enable', 'language', 'password_random');
        //Cria o Form Aura do Controller pra validar os campos enviados
        $form = new \App\Form\AdministratorUserForm(new \Aura\Input\Builder(), new \Aura\Input\Filter(), ['helper' => $this->container['helper']]);
        //Pega os parametros do request para passar no filtro
        $form->fill($data);
        //Se nao for válido os parametros enviado é exibido o erro no formulário
        if (!$form->filter()) {
            return $this->view($response, 'view/administrator/user/edit.twig', ['form' => $form]);
        }
        //Instancia o serviço de Administrator para o WS
        $service = new \App\Http\Service\AdministratorService($this->container);
        /* @var $service \App\Http\Service\AdministratorService */
        $wsData = $service->updateUser($data);
        if (!is_array($wsData)) {
            return;
        }
        if ($wsData['error'] === true) {
            $vars = [
                'form' => $form,
                'alert_error' => $wsData['localized_messages']
            ];
            return $this->view($response, 'view/administrator/user/edit.twig', $vars);
        }
        $vars = [
            'form' => $form,
            'alert_success' => [qrTranslate('UPDATE_SUCCESS')]
        ];
        return $this->view($response, 'view/administrator/user/edit.twig', $vars);
    }

    /**
     * @param $request
     * @param $response
     * @return \Slim\Http\Response
     */
    public function userNew($request, $response) {
        $form = new \App\Form\AdministratorUserInsertForm(new \Aura\Input\Builder(), new \Aura\Input\Filter(), ['helper' => $this->container['helper']]);
        $form->init();
        $args = [
            'form' => $form
        ];
        return $this->view($response, 'view/administrator/user/new.twig', $args);
    }

    /**
     * @param $request
     * @param $response
     * @param $args
     * @throws \Exception
     * @return \Slim\Http\Response
     */
    public function userInsert($request, $response) {
        if (false === $request->getAttribute('csrf_status')) {
            $msg_error = [
                'number' => 00214,
                'title' => $this->container['helper']->qrTranslate('VALIDATOR_ERROR_CSRF_TITLE'),
                'msg' => $this->container['helper']->qrTranslate('VALIDATOR_ERROR_CSRF_MESSAGE')
            ];

            $url = $this->container['router']->pathFor('error', [], $msg_error);
            return $response->withStatus(404)->withHeader('Location', $url);
        }
        //Pega os Parametros enviado pelo request e compacta em $data
        $fname = $this->getParam('fname');
        $lname = $this->getParam('lname');
        $occupation = $this->getParam('occupation');
        $email = $this->getParam('email');
        $id_group = $this->getParam('id_group');
        $password = $this->getParam('password');
        $password_random = $this->getParam('password_random');
        $enable = $this->getParam('enable');
        $language = $this->getParam('language');
        $data = compact('fname', 'lname', 'occupation', 'email', 'id_group', 'password', 'enable', 'language', 'password_random');

        //Cria o Form Aura do Controller pra validar os campos enviados
        $form = new \App\Form\AdministratorUserInsertForm(new \Aura\Input\Builder(), new \Aura\Input\Filter(), ['helper' => $this->container['helper']]);
        //Pega os parametros do request para passar no filtro
        $form->fill($data);
        //Se nao for válido os parametros enviado é exibido o erro no formulário
        if (!$form->filter()) {
            return $this->view($response, 'view/administrator/user/new.twig', ['form' => $form]);
        }

        //Instacia o serviço de Login para o WS
        $service = new \App\Http\Service\AdministratorService($this->container);
        /* @var $service \App\Http\Service\AdministratorService */

        $wsData = $service->insertUser($data);

        if (!is_array($wsData)) {
            return;
        }
        if ($wsData['error'] === true) {
            $vars = [
                'form' => $form,
                'alert_error' => $wsData['localized_messages']
            ];

            return $this->view($response, 'view/administrator/user/new.twig', $vars);
        }

        // Limpa o Formulário
        $form->init();

        $vars = [
            'form' => $form,
            'alert_success' => [qrTranslate('INSERT_SUCCESS')]
        ];

        return $this->view($response, 'view/administrator/user/new.twig', $vars);


    }

//</editor-fold>
//<editor-fold desc="PERMISSÕES DE TAREFAS">
    /**
     * @param $request
     * @param $response
     * @param $args
     * @return \Slim\Http\Response|void
     */
    public function taskPermissionIndex($request, $response, $args) {
        $args['table_buttons'] = [[
            'route' => 'administrator.task.permission.user',
            'route_params' => ['id_user' => "id_replace_url1"],
            'id_replace_url' => ['id_supervisor'],
            'button_class' => 'edit',
            'button_icon' => 'fa fa-key',
            'button_name' => 'BTN_PERMISSION',
        ]];
        return $this->view($response, 'view/administrator/task_permission/index.twig', $args);
    }

    public function taskPermissionList($request, $response) {
        //Instancia o serviço de Administrator para o WS
        $serviceGroup = new \App\Http\Service\AdministratorService($this->container);
        /* @var $serviceGroup \App\Http\Service\AdministratorService */

        $data_ws = [];

        //Pega todos os parametros para atualizar a consulta
        $offset = $this->getParam('offset', 0);
        $limit = $this->getParam('limit', 20);
        $search = $this->getParam('search', '');
        $sort = $this->getParam('sort', 'supervisor_name');
        $order = $this->getParam('order', 'asc');

        $data_ws = [
            'offset' => $offset,
            'limit' => $limit,
            'order_list' => $sort . ' ' . $order,
        ];
        if ($search) {
            $data_ws['filter'] = 'filtro_geral:' . $search;
        }

        try {
            //Faz o envio e pega a resposta no WS
            $wsData = $serviceGroup->listTaskPermissions($data_ws);

            //Retorna pra view os dados
            $json = json_encode($wsData['data']);
            return $json;
        }
        catch (\Exception $e) {
            syslog(LOG_ERR, $e->getMessage());
        }
    }

    /**
     * @param $request
     * @param $response
     * @throws \Exception
     * @return \Slim\Http\Response
     */
    public function userPermissions($request, $response) {
        $id_user = $request->getAttribute('id_user');
        $service = new \App\Http\Service\AdministratorService($this->container);
        $wsData = $service->getUser(['id_user' => $id_user]);
        if ($wsData['error'] === true) {
            $vars = [
                'alert_error' => $wsData['localized_messages']
            ];
            return $this->view($response, 'view/administrator/task_permission/index.twig', $vars);
        }
        $args = [
            'id_supervisor' => $id_user,
            'supervisor_name' => trim(trim($wsData['data']['fname']) . ' ' . trim($wsData['data']['lname'])),
        ];
        return $this->view($response, 'view/administrator/task_permission/user.twig', $args);
    }

    public function taskUserPermissionList($request, $response) {
        $service = new \App\Http\Service\AdministratorService($this->container);

        //Pega todos os parametros para atualizar a consulta
        $id_supervisor = $this->getParam('id_supervisor');
        $offset = $this->getParam('offset', 0);
        $limit = $this->getParam('limit', 20);
        $search = $this->getParam('search', '');
        $sort = $this->getParam('sort', 'executor_name');
        $order = $this->getParam('order', 'asc');

        $data_ws = [
            'id_supervisor' => $id_supervisor,
            'offset' => $offset,
            'limit' => $limit,
            'order_list' => $sort . ' ' . $order,
        ];
        if ($search) {
            $data_ws['filter'] = 'filtro_geral:' . $search;
        }

        try {
            //Faz o envio e pega a resposta no WS
            $wsData = $service->listUserTaskPermissions($data_ws);

            //Retorna pra view os dados
            $json = json_encode($wsData['data']);
            return $json;
        }
        catch (\Exception $e) {
            syslog(LOG_ERR, $e->getMessage());
        }
    }

    /**
     * @param $request
     * @param $response
     * @param $args
     * @throws \Exception
     * @return \Slim\Http\Response
     */
    public function taskPermissionUpdate($request, $response) {
        try {
            //Pega os Parametros enviado pelo request e compacta em $data
            $id_supervisor = $this->getParam('id_supervisor');
            $id_executor = $this->getParam('id_executor');
            $task_permission_type = $this->getParam('task_permission_type');

            $data = compact('id_supervisor', 'id_executor', 'task_permission_type');
            //Cria o Form Aura do Controller pra validar os campos enviados
            $form = new \App\Form\AdministratorTaskPermissionForm(new \Aura\Input\Builder(), new \Aura\Input\Filter(), ['helper' => $this->container['helper']]);
            //Pega os parametros do request para passar no filtro
            $form->fill($data);
            //Se nao for válido os parametros enviado é exibido o erro no formulário
            if (!$form->filter()) {
                return json_encode([
                    'error' => true,
                    'messages' => [
                        $this->container['helper']->qrTranslate('VALIDATION_ERROR')
                    ],
                ]);
            }

            $service = new \App\Http\Service\AdministratorService($this->container);
            $wsData = $service->updateTaskPermission($data);
            if (!is_array($wsData)) {
                return;
            }
            if ($wsData['error'] === true) {
                return json_encode([
                    'error' => true,
                    'messages' => $wsData['localized_messages'],
                ]);
            }

            return json_encode([
                'error' => false,
                'messages' => [],
            ]);
        }
        catch(\Exception $e) {
            return json_encode([
                'error' => true,
                'messages' => [$e->getMessage()],
            ]);
        }
    }
//</editor-fold>

}