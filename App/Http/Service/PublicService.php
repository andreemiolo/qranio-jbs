<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Service;

/**
 * Description of LoginService
 *
 * @author gianmenezes
 */
class PublicService extends BaseService {
    
    /**
     * Verifica o login do usuário usando uma conta do Qranio
     * @param array $data Parâmetros a serem enviados
     * @return array
     */
    public function getUserByToken(array $data)
    {
        return $this->sendRequest('rest/v1/cerebelo/configuration/user/getUserByToken', $data);
    }
    
    /**
     * Verifica o login do usuário usando uma conta do Qranio
     * @param array $data Parâmetros a serem enviados
     * @return array
     */
    public function recoveryPassword(array $data)
    {
        return $this->sendRequest('rest/v2/cerebelo/configuration/user/forgot', $data);
    }
    
}
