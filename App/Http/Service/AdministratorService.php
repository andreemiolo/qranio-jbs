<?php
/**
 * Created by PhpStorm.
 * User: gianmenezes
 * Date: 21/09/17
 * Time: 17:17
 */

namespace App\Http\Service;


/**
 * Class AdministratorService
 * - Utilizada para fazer os requests relacionado ao módulo de administração do Cerebelo
 * @package App\Http\Service
 * @author gianmenezes
 */

class AdministratorService extends BaseService
{

    //<editor-fold desc="Grupos">
    /**
     * Lista os grupos cadastrados no cerebelo
     * @param array $data Parâmetros a serem enviados
     * @return array
     */
    public function listGroupsCerebelo(array $data) {
        return $this->sendRequest('rest/v2/cerebelo/configuration/group/list', $data);
    }

    /**
     * Envia para o ws as informacões para Editar o grupo
     * @param array $data Parâmetros a serem enviados
     * @return array
     */
    public function getGroupCerebelo(array $data) {
        return $this->sendRequest('rest/v2/cerebelo/configuration/group/get', $data);
    }

    /**
     * Envia para o ws as informacões para adicionar o grupo
     * @param array $data Parâmetros a serem enviados
     * @return array
     */
    public function insertGroupCerebelo(array $data) {
        return $this->sendRequest('rest/v2/cerebelo/configuration/group/insert', $data);
    }

    /**
     * Envia para o ws as informacões para Editar o grupo
     * @param array $data Parâmetros a serem enviados
     * @return array
     */
    public function updateGroupCerebelo(array $data) {
        return $this->sendRequest('rest/v2/cerebelo/configuration/group/update', $data);
    }

    /**
     * Lista as permissões a serem dadas
     * @param array $data Parâmetros a serem enviados
     * @return array
     */
    public function listPermissions(array $data) {
        return $this->sendRequest('rest/v2/cerebelo/configuration/permission/get', $data);
    }

    /**
     * Atualiza as permissões de um grupo
     * @param array $data Parâmetros a serem enviados
     * @return array
     */
    public function updatePermissions(array $data) {
        return $this->sendRequest('rest/v2/cerebelo/configuration/permission/update', $data);
    }
    //</editor-fold>
    //<editor-fold desc="Usuários">
    /**
     * Lista os usuários cadastrados no cerebelo
     * @param array $data Parâmetros a serem enviados
     * @return array
     */
    public function listUsers(array $data) {
        return $this->sendRequest('rest/v2/cerebelo/configuration/user/list', $data);
    }

    /**
     * Pega do ws as informacões do usuário
     * @param array $data Parâmetros a serem enviados
     * @return array
     */
    public function getUser(array $data) {
        return $this->sendRequest('rest/v2/cerebelo/configuration/user/get', $data);
    }

    /**
     * Envia para o ws as informacões para Editar a rota
     * @param array $data Parâmetros a serem enviados
     * @return array
     */
    public function updateUser(array $data) {
        return $this->sendRequest('rest/v2/cerebelo/configuration/user/update', $data);
    }

    /**
     * Envia para o ws as informacões para adicionar o usuário
     * @param array $data Parâmetros a serem enviados
     * @return array
     */
    public function insertUser(array $data) {
        return $this->sendRequest('rest/v2/cerebelo/configuration/user/insert', $data);
    }
    //</editor-fold>
    //<editor-fold desc="Permissões de Tarefas">
    /**
     * Lista as permissões cadastradas no cerebelo
     * @param array $data Parâmetros a serem enviados
     * @return array
     */
    public function listTaskPermissions(array $data) {
        return $this->sendRequest('rest/v2/cerebelo/task/permissions', $data);
    }

    /**
     * Lista as permissões de um usuário
     * @param array $data Parâmetros a serem enviados
     * @return array
     */
    public function listUserTaskPermissions(array $data) {
        return $this->sendRequest('rest/v2/cerebelo/task/permissions/user', $data);
    }

    /**
     * Atualiza uma permissão específica
     * @param array $data Parâmetros a serem enviados
     * @return array
     */
    public function updateTaskPermission(array $data) {
        return $this->sendRequest('rest/v2/cerebelo/task/permissions/update', $data);
    }
    //</editor-fold>

}