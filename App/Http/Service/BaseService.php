<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Service;

/**
 * Description of BaseService
 *
 * @author gianmenezes
 */
class BaseService {

    /**
     * @var Slim\Container
     */
    protected $container;

    public function __construct($container) {
        $this->container = $container;
    }

    /**
     * Envia a requisição e retorna a resposta do serviço
     * @param \GuzzleHttp\Client $request
     * @return array
     */
    protected function  generateTokenForQranio($request, $params, $uri) {
        try {

            $data = $this->container->get('settings')['app'];
            foreach ($params as $key => $value) {
                $data[$key] = $value;
            }

            // ADICIONA NONCE E TIMESTAMP PARA CADA REQUISICAO
            $data['timestamp'] =  (string) (new \DateTime())->getTimestamp();
            $data['nonce'] = (string) mt_rand();


//            $raw_data = '';
//            foreach ($data as $key => $value) {
//                $raw_data .= '&' . rawurlencode($key) . '=' . rawurlencode($value);
//            }
//
//            $data_r = substr($raw_data, 1);
            $data_r = http_build_query($data);
            $hash = hash_hmac('sha256', $data_r, $this->container->get('settings')['service']['pvtId']);

            $options = [
                'auth'        => [$this->container->get('settings')['service']['apiId'], $hash],
                'form_params' => $data
            ];

            $response = $request->post($uri, $options);
            return $response_json = \GuzzleHttp\json_decode($response->getBody()->getContents(), true);
        }
        catch (\GuzzleHttp\Exception\RequestException $exc) {

            // Verifica se a resposta do WS é erro experado
            if ($exc->hasResponse() && $exc->getResponse()->getStatusCode() && $exc->getResponse()->getStatusCode() === 500) {
                $body = $exc->getResponse()->getBody()->getContents();
                $json = \GuzzleHttp\json_decode($body, true);
                return $json;
            }

            if (!$exc->hasResponse() || !$exc->getResponse()->getStatusCode()) {
                $msg_error = [
                    'number' => $exc->getCode(),
                    'title'  => $exc->getHandlerContext()['error'],
                    'msg'    => $exc->getMessage()
                ];

                return $this->container->view->render($this->container['response'], 'view/public/error.twig', $msg_error)->withStatus(500);
            }

            if ($exc->getResponse()->getStatusCode() <> 500) {

                $msg_error = [
                    'number' => $exc->getResponse()->getStatusCode(),
                    'title'  => $exc->getResponse()->getReasonPhrase(),
                    'msg'    => $exc->getResponse()->getBody()->getContents()
                ];
                return $this->container->view->render($this->container['response'], 'view/public/error.twig', $msg_error)->withStatus($exc->getResponse()->getStatusCode());
            }
        }
    }


    /**
     * @param $uri
     * @param $data
     * @return array
     */
    public function sendRequest($uri, $data) {

        // Adiciona o ID do usuário logado pra fazer as requisições
        if(isset($this->container->get('session')['user_id'])) {
            if (!isset($data['id_user'])) {
                $data['id_user'] = $this->container->get('session')['user_id'];
            }
            $data['language'] = $this->container->get('session')['user_language'];
        }

        $settings = $this->container->get('settings')['service'];
        $config_client = [
            'base_uri' => $settings['baseUrl'] . $settings['basePath']
        ];

        $request = new \GuzzleHttp\Client($config_client);

        $response = $this->generateTokenForQranio($request, $data, $uri);

        return $response;
    }

}
