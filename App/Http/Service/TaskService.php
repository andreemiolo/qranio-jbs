<?php
/**
 * Created by PhpStorm.
 * User: gianmenezes
 * Date: 21/09/17
 * Time: 17:17
 */

namespace App\Http\Service;


/**
 * Class TaskService
 * - Utilizada para fazer os requests relacionado ao módulo de Tarefa do Cerebelo
 * @package App\Http\Service
 * @author gianmenezes
 */

class TaskService extends BaseService {

    /**
     * Lista as tarefas
     * @param array $data Parâmetros a serem enviados
     * @return array
     */
    public function listTasks(array $data) {
        return $this->sendRequest('rest/v2/cerebelo/task/list', $data);
    }

    /**
     * Lista os usuários supervisionados por um supervisor
     * @param array $data Parâmetros a serem enviados
     * @return array
     */
    public function listSupervisedUsers(array $data) {
        return $this->sendRequest('rest/v2/cerebelo/task/supervised', $data);
    }

    /**
     * Insere uma tarefa
     * @param array $data Parâmetros a serem enviados
     * @return array
     */
    public function insertTask(array $data) {
        return $this->sendRequest('rest/v2/cerebelo/task/insert', $data);
    }

    /**
     * Retorna os dados de uma tarefa
     * @param array $data Parâmetros a serem enviados
     * @return array
     */
    public function getTask(array $data) {
        return $this->sendRequest('rest/v2/cerebelo/task/get', $data);
    }

    /**
     * Atualiza uma tarefa
     * @param array $data Parâmetros a serem enviados
     * @return array
     */
    public function updateTask(array $data) {
        return $this->sendRequest('rest/v2/cerebelo/task/update', $data);
    }

    /**
     * Insere um andamento em uma tarefa
     * @param array $data Parâmetros a serem enviados
     * @return array
     */
    public function insertTaskProgress(array $data) {
        return $this->sendRequest('rest/v2/cerebelo/task/progress/insert', $data);
    }

    /**
     * Transfere uma tarefa
     * @param array $data Parâmetros a serem enviados
     * @return array
     */
    public function transferTask(array $data) {
        return $this->sendRequest('rest/v2/cerebelo/task/transfer', $data);
    }

    /**
     * Encerra uma tarefa
     * @param array $data Parâmetros a serem enviados
     * @return array
     */
    public function finishTask(array $data) {
        return $this->sendRequest('rest/v2/cerebelo/task/finish', $data);
    }

    /**
     * Reabre uma tarefa
     * @param array $data Parâmetros a serem enviados
     * @return array
     */
    public function reopenTask(array $data) {
        return $this->sendRequest('rest/v2/cerebelo/task/reopen', $data);
    }

}