<?php
/**
 * Created by PhpStorm.
 * User: gianmenezes
 * Date: 21/09/17
 * Time: 17:17
 */

namespace App\Http\Service;


/**
 * Class UserService
 * - Utilizada para fazer os requests relacionado ao módulo de usuário do APP
 * @package App\Http\Service
 * @author gianmenezes
 */

class UserService extends BaseService
{

    /**
     * Lista os usuários cadastrados no APP
     * @param array $data Parâmetros a serem enviados
     * @return array
     */
    public function listUserApp(array $data)
    {
        return $this->sendRequest('rest/v2/cerebelo/users/qranio/list', $data);
    }

}