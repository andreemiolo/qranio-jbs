<?php

return [
    'settings' => [
        'app'                               => [
            'XDEBUG_PROFILE' => getenv('XDEBUG_PROFILE')? : 0,
            'language'       => getenv('LANGUAGE_DEFAULT'),
            'version'        => getenv('VERSION'),
            'origin'         => 'CEREBELO',
            'salt'           => 'KTWBjmbZUTm39soQcPtHB0xtEgka8JlVsDCFNFKKjBVTTW8RxifwpyRheih67HG'
        ],
        'displayErrorDetails'               => true, // set to false in production
        'addContentLengthHeader'            => true, // Allow the web server to send the content-length header
        'determineRouteBeforeAppMiddleware' => true,
        // Renderer settings
        'renderer'                          => [
            'template_path'     => QR_DIR_BASE . '/App/Templates/',
            'public_path'       => QR_DIR_BASE . '/public/',
            'cache_path'        => QR_DIR_BASE . '/storage/cache',
            'translations_path' => QR_DIR_BASE . '/App/Language/'
        ],
        'service'                           => [
            'apiId'    => getenv('API_ID'),
            'pvtId'    => getenv('PVT_ID'),
            'baseUrl'  => getenv('BASE_URL'),
            'basePath' => getenv('BASE_PATH'),
        ],
        // Monolog settings
        'logger'                            => [
            'name'  => 'Cerebelo',
            'path'  => QR_DIR_BASE . '/logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ]
    ]
];
