<?php

// Rotas Públicas sem estar autenticadas
$app->get('/assets[/{f:.*}]', '\App\Http\Controller\PublicController:asset')->setName('asset');
$app->get('/[index]', '\App\Http\Controller\PublicController:index')->setName('index');
$app->get('/logout', '\App\Http\Controller\PublicController:logout')->setName('logout');
$app->get('/error', '\App\Http\Controller\PublicController:error')->setName('error');
$app->map(['GET', 'POST'], '/recovery-password', '\App\Http\Controller\PublicController:recoveryPassword')->setName('recovery.password');

//Acões de usuários não logados
$app->group('/no-auth', function () use ($app) {
    $app->get('/new-password/{id}/{token}', '\App\Http\Controller\PublicController:newPassword')->setName('new.password');
    $app->get('/confirm-account/{id}/{token}', '\App\Http\Controller\PublicController:confirmAccount')->setName('confirm.account');
});

//Login
$app->post('/[index]', '\App\Http\Controller\LoginController:loginApp')->setName('login');
$app->get('/temporary', '\App\Http\Controller\LoginController:temporaryPassword')->setName('temporary.password');
$app->get('/expired', '\App\Http\Controller\LoginController:expiredPassword')->setName('expired.password');
$app->post('/redefine-password', '\App\Http\Controller\LoginController:redefinePassword')->setName('redefine.password');

//Home
$app->get('/home', '\App\Http\Controller\HomeController:home')->setName('home');

//Dashboard
$app->get('/dashboard', '\App\Http\Controller\DashboardController:general')->setName('dashboard.general');

//Tasks
$app->group('/task', function () use ($app) {
    $app->get('/[index]', '\App\Http\Controller\TaskController:taskIndex')->setName('task.index');
    $app->post('/list', '\App\Http\Controller\TaskController:taskList')->setName('task.list');
    $app->get('/view/{id_task}', '\App\Http\Controller\TaskController:taskView')->setName('task.view');
    $app->get('/new', '\App\Http\Controller\TaskController:taskNew')->setName('task.new');
    $app->post('/insert', '\App\Http\Controller\TaskController:taskInsert')->setName('task.insert');
    $app->post('/update', '\App\Http\Controller\TaskController:taskUpdate')->setName('task.update');
    $app->post('/progress/insert', '\App\Http\Controller\TaskController:taskProgressInsert')->setName('task.progress.insert');
    $app->post('/transfer', '\App\Http\Controller\TaskController:taskTransfer')->setName('task.transfer');
    $app->post('/finish', '\App\Http\Controller\TaskController:taskFinish')->setName('task.finish');
    $app->post('/reopen', '\App\Http\Controller\TaskController:taskReopen')->setName('task.reopen');
});

//Conta do usuário Cerebelo
$app->group('/account', function () use ($app) {
    $app->get('/profile', '\App\Http\Controller\AccountController:profile')->setName('account.profile');
    $app->get('/profile-edit', '\App\Http\Controller\AccountController:profileEdit')->setName('account.profile.edit');
});

//Funcoes do administrador
$app->group('/administrator', function () use ($app) {
    $app->get('/group', '\App\Http\Controller\AdministratorController:groupIndex')->setName('administrator.group.index');
    $app->post('/group/list', '\App\Http\Controller\AdministratorController:groupListTable')->setName('administrator.group.list');
    $app->get('/group/new', '\App\Http\Controller\AdministratorController:groupNew')->setName('administrator.group.new');
    $app->post('/group/insert', '\App\Http\Controller\AdministratorController:groupInsert')->setName('administrator.group.insert');
    $app->get('/group/edit/{id_group}', '\App\Http\Controller\AdministratorController:groupEdit')->setName('administrator.group.edit');
    $app->post('/group/update', '\App\Http\Controller\AdministratorController:groupUpdate')->setName('administrator.group.update');
    $app->get('/group/view/{id_group}', '\App\Http\Controller\AdministratorController:groupNew')->setName('administrator.group.view');
    $app->get('/group/permission/{id_group}', '\App\Http\Controller\AdministratorController:groupPermission')->setName('administrator.group.permission');
    $app->post('/group/permission/update', '\App\Http\Controller\AdministratorController:groupPermissionUpdate')->setName('administrator.group.permission.update');
    $app->get('/user', '\App\Http\Controller\AdministratorController:userIndex')->setName('administrator.user.index');
    $app->post('/user/list', '\App\Http\Controller\AdministratorController:userList')->setName('administrator.user.list');
    $app->get('/user/view/{id_user}', '\App\Http\Controller\AdministratorController:userView')->setName('administrator.user.view');
    $app->get('/user/edit/{id_user}', '\App\Http\Controller\AdministratorController:userEdit')->setName('administrator.user.edit');
    $app->post('/user/update', '\App\Http\Controller\AdministratorController:userUpdate')->setName('administrator.user.update');
    $app->get('/user/new', '\App\Http\Controller\AdministratorController:userNew')->setName('administrator.user.new');
    $app->post('/user/insert', '\App\Http\Controller\AdministratorController:userInsert')->setName('administrator.user.insert');
    $app->get('/task/permission', '\App\Http\Controller\AdministratorController:taskPermissionIndex')->setName('administrator.task.permission.index');
    $app->post('/task/permission/list', '\App\Http\Controller\AdministratorController:taskPermissionList')->setName('administrator.task.permission.list');
    $app->get('/task/permission/{id_user}', '\App\Http\Controller\AdministratorController:userPermissions')->setName('administrator.task.permission.user');
    $app->post('/task/permission/user', '\App\Http\Controller\AdministratorController:taskUserPermissionList')->setName('administrator.task.permission.user.list');
    $app->post('/task/permission/update', '\App\Http\Controller\AdministratorController:taskPermissionUpdate')->setName('administrator.task.permission.update');
});


//Usuários do APP
$app->group('/user', function () use ($app) {
    $app->get('/list', '\App\Http\Controller\UserController:userList')->setName('app.user.index');
    $app->post('/list-table', '\App\Http\Controller\UserController:userListTable')->setName('app.user.list');
});

