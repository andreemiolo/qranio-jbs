<?php

return array(
//<editor-fold desc="MENSAGENS DE ALERTAS">
//============================================================================//
// MENSAGENS DE ALERTAS
//============================================================================//
'ALERT_ERROR' => 'Erro!',
'ALERT_SUCCESS' => 'Sucesso!',
'INSERT_SUCCESS' => 'Inserido com sucesso!',
'UPDATE_SUCCESS' => 'Atualizado com sucesso!',
'DELETE_SUCCESS' => 'Excluído com sucesso!',
'ERROR_MSG_TRY_AGAIN' => 'Erro inesperado, tente novamente!',
'MSG_RECORD_DELETED' => 'Registro excluído com sucesso!',
'MSG_RECORD_DELETE_CONFIRM' => 'Deseja realmente apagar o registro %s?',
//</editor-fold>
//<editor-fold desc="BOTÕES">
//============================================================================//
//BOTÕES
//============================================================================//
'BTN_ENTER' => 'Entrar',
'BTN_SEND' => 'Enviar',
'BTN_CANCEL' => 'Cancelar',
'BTN_CHANGE' => 'Alterar',
'BTN_CONFIRM' => 'Confirmar',
'BTN_ADD' => 'Adicionar',
'BTN_VIEW' => 'Visualizar',
'BTN_EDIT' => 'Editar',
'BTN_SAVE' => 'Salvar',
'BTN_UPDATE' => 'Atualizar',
'BTN_PERMISSION' => 'Permissões',
'BTN_DELETE' => 'Excluir',
'BTN_FINISH' => 'Encerrar',
'BTN_TRANSFER' => 'Transferir',
'BTN_REOPEN' => 'Reabrir',
//</editor-fold>
//<editor-fold desc="MENU">
//============================================================================//
//MENU
//============================================================================//
'MENU_HOME' => 'Home',
'MENU_TASKS' => 'Tarefas',
'MENU_MESSAGES' => 'Mensagens',
'MENU_DASHBOARD' => 'Dashboard',
'MENU_APP_USERS' => 'Usuários do App',
'MENU_COURSE' => 'Trilhas',
'MENU_MANAGE_COURSES' => 'Trilhas',
'MENU_COURSE_SUBCATEGORIES' => 'Subcategorias',
'MENU_COURSE_STAGES' => 'Estágios',
'MENU_LIBRARY' => 'Biblioteca Virtual',
'MENU_DIGITAL_SHELF' => 'Prateleira Digital',
'MENU_QUESTIONS' => 'Perguntas',
'MENU_REPORTED_QUESTIONS' => 'Reportadas',
'MENU_RECEIVED_QUESTIONS' => 'Recebidas',
'MENU_QUESTION_CATEGORIES' => 'Categorias',
'MENU_MANAGE_QUESTIONS' => 'Perguntas',
'MENU_QUESTION_IMPORT' => 'Importar via Planilha',
'MENU_PERIODIC_QUESTIONS' => 'Pergunta Periódica',
'MENU_MANAGE_PERIODIC_QUESTIONS' => 'Perguntas',
'MENU_REPORTED_PERIODIC_QUESTIONS' => 'Reportadas',
'MENU_PRIZES' => 'Prêmios',
'MENU_APPROVE_PARTNER' => 'Aprovar Parceiro',
'MENU_APPROVE_PRIZES' => 'Aprovar Prêmios',
'MENU_REDEEMS' => 'Resgates',
'MENU_PRIZE_CATEGORIES' => 'Categorias',
'MENU_MANAGE_PRIZES' => 'Prêmios',
'MENU_PARTNERS' => 'Parceiros',
'MENU_SITE' => 'Site',
'MENU_PRESS' => 'Matérias',
'MENU_APP' => 'Aplicativos',
'MENU_NOTIFICATIONS' => 'Notificações',
'MENU_FINANCES' => 'Financeiro',
'MENU_CASHIER' => 'Caixa',
'MENU_PAYABLES' => 'Contas a Pagar',
'MENU_RECEIVABLES' => 'Contas a Receber',
'MENU_FINANCE_REPORT' => 'Relatórios',
'MENU_CASHIER_REPORT' => 'Caixa',
'MENU_PAYABLES_REPORT' => 'Contas a Pagar',
'MENU_RECEIVABLES_REPORT' => 'Contas a Receber',
'MENU_PROJECTED_CASHIER' => 'Caixa Projetado',
'MENU_PROJECTIONS_REPORT' => 'Projeções',
'MENU_FINANCE_SETTINGS' => 'Configurações',
'MENU_INCOME_TYPES' => 'Tipos de Receita',
'MENU_CREDIT_EXPENSE_TYPES' => 'Tipos de Crédito e Despesa',
'MENU_BANK_ACCOUNTS' => 'Contas Bancárias',
'MENU_PROJECTIONS' => 'Projeções',
'MENU_PLAN_ACCOUNTS' => 'Plano de Contas',
'MENU_PLAN_ACCOUNTS_1' => 'Plano de Contas 1',
'MENU_PLAN_ACCOUNTS_2' => 'Plano de Contas 2',
'MENU_PLAN_ACCOUNTS_3' => 'Plano de Contas 3',
'MENU_SUPPLIERS' => 'Fornecedores',
'MENU_REPORTS' => 'Relatórios',
'MENU_USER_REPORT' => 'Usuários',
'MENU_SINGLE_PLAYER_REPORT' => 'Individual',
'MENU_MULTIPLAYER_REPORT' => 'Duelos',
'MENU_PERIODIC_QUESTION_REPORT' => 'Pergunta do Dia',
'MENU_ADMINISTRATION' => 'Administração',
'MENU_ADMINISTRATION_GROUPS' => 'Grupos de Usuários',
'MENU_ADMINISTRATION_USERS' => 'Usuários',
'MENU_TASK_PERMISSIONS' => 'Permissões de Tarefas',
//</editor-fold>

//<editor-fold desc="AÇÕES">
//============================================================================//
//AÇÕES
//============================================================================//
//<editor-fold desc="TAREFAS">
'ACTION_INSERT_TASK' => 'Inserir',
'ACTION_UPDATE_TASK' => 'Editar',
//</editor-fold>
//<editor-fold desc="ADMINISTRATOR">
//<editor-fold desc="GRUPOS">
'ACTION_VIEW_CEREBELO_GROUPS' => 'Visualizar',
'ACTION_INSERT_CEREBELO_GROUPS' => 'Inserir',
'ACTION_UPDATE_CEREBELO_GROUPS' => 'Editar',
'ACTION_PERMIT_CEREBELO_GROUPS' => 'Permitir',
//</editor-fold>
//<editor-fold desc="USUÁRIOS">
'ACTION_VIEW_CEREBELO_USERS' => 'Visualizar',
'ACTION_INSERT_CEREBELO_USERS' => 'Inserir',
'ACTION_UPDATE_CEREBELO_USERS' => 'Editar',
//</editor-fold>
//<editor-fold desc="PERMISSÕES DE TAREFAS">
'ACTION_MANAGE_TASK_PERMISSIONS' => 'Gerenciar',
//</editor-fold>
//</editor-fold>
//</editor-fold>
//<editor-fold desc="LABELS DE FORMS">
//============================================================================//
// LABELS DE FORMS
//============================================================================//
'LABEL_ID' => 'ID',
'LABEL_USER' => 'Usuário',
'LABEL_EMAIL' => 'Email',
'LABEL_PASSWORD' => 'Senha',
'LABEL_PASSWORD_NEW' => 'Nova Senha',
'LABEL_PASSWORD_CONFIRM' => 'Redigite a Senha',
'LABEL_RANDOM_PASSWORD' => 'Senha Aleatória',
'LABEL_NAME' => 'Nome',
'LABEL_LAST_NAME' => 'Sobrenome',
'LABEL_OCCUPATION' => 'Cargo',
'LABEL_REGISTER' => 'Cadastro',
'LABEL_PREMIUM' => 'Premium',
'LABEL_COUNTRY' => 'País',
'LABEL_ENABLED' => 'Habilitado',
'LABEL_GROUP' => 'Grupo',
'LABEL_LANGUAGE' => 'Idioma',
'LABEL_PERMISSIONS' => 'Permissões',
'LABEL_PERMISSION_TYPE' => 'Tipo',
'LABEL_EXECUTOR' => 'Executor',
'LABEL_OPERATOR' => 'Operador',
'LABEL_DESCRIPTION' => 'Descrição',
'LABEL_TASK_INITIAL_DATE' => 'Data Inicial da Tarefa',
'LABEL_TASK_FINAL_DATE' => 'Data Limite da Tarefa',
'LABEL_TASK_STATUS' => 'Status da Tarefa',
'LABEL_REGISTER_DATE' => 'Data de Cadastro',
'LABEL_TASK_TIME_INTERVAL' => 'Período',
'LABEL_TASK_PROGRESS' => 'Andamento',
//</editor-fold>
//<editor-fold desc="OPTIONS SELECT">
//============================================================================//
// OPTIONS SELECT
//============================================================================//
'OPTION_YES' => 'Sim',
'OPTION_NO' => 'Não',
'OPTION_ALL' => 'Todos',
'OPTION_SELECT' => 'Selecione',
'OPTION_PORTUGUESE_BR' => 'Português',
'OPTION_ENGLISH' => 'Inglês',
'OPTION_TASK_PERMISSION_NONE' => 'Nenhuma',
'OPTION_TASK_PERMISSION_D' => 'Direta',
'OPTION_TASK_PERMISSION_T' => 'Total',
//</editor-fold>
//<editor-fold desc="PARTIAL">
//============================================================================//
// PARTIAL
//============================================================================//
'BREADCRUMB_HERE' => 'VOCÊ ESTÁ AQUI',
'HEADER_NOTIFICATIONS' => 'Notificações',
'HEADER_MY_ACCOUNT' => 'Minha Conta',
'HEADER_ADMINISTRATOR_FUNCTIONS' => 'Funções Administrativa',
'HEADER_EXIT' => 'Sair',
'SIDE_BAR_BROWNSER' => 'Menu',
'HEADER_VIEW' => 'Visualizar',
'HEADER_EDIT' => 'Editar',
'HEADER_NEW' => 'Novo',
'HEADER_LIST' => 'Listagem',
//</editor-fold>
//<editor-fold desc="TEXTOS DE PAGINAS">
//============================================================================//
// TEXTOS DE PAGINAS
//============================================================================//
//<editor-fold desc="GENÉRICOS">
//***************************//
// GENÉRICOS
//**************************//
'TITLE_ADD' => 'Adicionar',
'TITLE_EDIT' => 'Editar',
'TITLE_VIEW' => 'Visualizar',
'VALIDATION_ERROR' => 'Parâmetros insuficientes',
//</editor-fold>
//<editor-fold desc="LOGIN">
//***************************//
// LOGIN
//**************************//
'LOGIN_DESCRIPTION' => 'Use seu login de acesso para entrar no painel administrativo da sua empresa.',
'LOGIN_FORGOT_PASSWORD' => 'Esqueceu a Senha?',
//</editor-fold>
//<editor-fold desc="RECOVERY PASSWORD">
//***************************//
// RECOVERY PASSWORD
//**************************//
'RECOVERY_PASSWORD_TITLE' => 'Recuperar Senha',
'RECOVERY_PASSWORD_HELLO' => 'Olá',
'RECOVERY_PASSWORD_DESCRIPTION' => 'Digite o seu email cadastrado recuperar sua senha.',
//</editor-fold>
//<editor-fold desc="REDEFINE PASSWORD">
//***************************//
// REDEFINE PASSWORD
//**************************//
'TEMPORARY_PASSWORD_TITLE' => 'Redefinir Senha',
'TEMPORARY_PASSWORD_HELLO' => 'Olá',
'TEMPORARY_PASSWORD_DESCRIPTION' => 'Para acessar o sistema administrativo defina uma senha.',
'EXPIRED_PASSWORD_TITLE' => 'Redefinir Senha',
'EXPIRED_PASSWORD_HELLO' => 'Olá',
'EXPIRED_PASSWORD_DESCRIPTION' => 'Sua senha expirou, redefina sua senha para continuar.',
//</editor-fold>
//<editor-fold desc="NEW PASSWORD">
//***************************//
// NEW PASSWORD
//**************************//
'NEW_PASSWORD_TITLE' => 'Nova Senha',
'NEW_PASSWORD_HELLO' => 'Olá',
'NEW_PASSWORD_DESCRIPTION' => 'Digite a nova senha que deseja utilizar para acessar a aplicação',
//</editor-fold>
//<editor-fold desc="CONFIRM ACCOUNT">
//***************************//
// CONFIRM ACCOUNT
//**************************//
'CONFIRM_ACCOUNT_TITLE' => 'Confirmar Conta',
'CONFIRM_ACCOUNT_HELLO' => 'Olá',
'CONFIRM_ACCOUNT_DESCRIPTION' => 'Digite a senha que deseja utilizar para acessar a aplicação',
//</editor-fold>
//<editor-fold desc="HOME">
//***************************//
// HOME
//**************************//
'HOME_TITLE' => 'Página Principal',
'HOME_WIDGET_USER_TITLE' => 'Usuários',
'HOME_WIDGET_USER_DESCRIPTION' => 'Gerencie os seus usuários e veja o progresso detalhado',
'HOME_WIDGET_DASHBOARD_TITLE' => 'Dashboard',
'HOME_WIDGET_DASHBOARD_DESCRIPTION' => 'Acompanhe cadastros, usuários ativos entre outras',
'HOME_WIDGET_COURSE_TITLE' => 'Cursos',
'HOME_WIDGET_COURSE_DESCRIPTION' => 'Adicione, renomeie e gerencie seus cursos cadastrados',
'HOME_WIDGET_QUESTION_TITLE' => 'Perguntas',
'HOME_WIDGET_QUESTION_DESCRIPTION' => 'Adicione, altere e gerencie as perguntas da plataforma',
'HOME_WIDGET_PERIODIC_QUESTION_TITLE' => 'Pergunta do Dia',
'HOME_WIDGET_PERIODIC_QUESTION_DESCRIPTION' => 'Adicione, altere e gerencie as Perguntas do Dia da Plataforma',
'HOME_WIDGET_ADMINISTRATOR_TITLE' => 'Funções do Administrador',
'HOME_WIDGET_ADMINISTRATOR_DESCRIPTION' => 'Gerencie usuários, grupos e permissão de acesso',
//</editor-fold>
//<editor-fold desc="ADMINISTRATOR">
//***************************//
// ADMINISTRATOR
//**************************//
'ADMINISTRATOR_TITLE' => 'Administração',
//<editor-fold desc="GRUPOS">
//***************************//
// GRUPOS
//**************************//
'GROUP_TITLE' => 'Grupos de Usuários',
'GROUP_LIST' => 'Grupo de Usuários',
'GROUP_ADD' => 'Adicionar',
'GROUP_ADD_GROUP_USER' => 'Adicionar Grupo de Usuários',
'GROUP_EDIT' => 'Editar',
'GROUP_EDIT_GROUP_USER' => 'Editar Grupo de Usuários',
'GROUP_PERMISSION' => 'Permissão',
'GROUP_PERMISSION_GROUP_USER' => 'Permissão de Grupo de Usuários',
'GROUP_ALREADY_EXISTS' => 'Grupo já existe',
//</editor-fold>
//<editor-fold desc="USUÁRIOS">
//***************************//
// USUÁRIOS
//**************************//
'USER_TITLE' => 'Usuários',
'TITLE_ADD_USER' => 'Adicionar Usuário',
'TITLE_EDIT_USER' => 'Editar Usuário',
//</editor-fold>
//<editor-fold desc="PERMISSÕES DE TAREFAS">
//***************************//
// PERMISSÕES DE TAREFAS
//**************************//
'TASK_PERMISSION_TITLE' => 'Permissões de tarefas',
'MULTIPLE_PERMISSIONS' => '%s Permissões',
'TASK_PERMISSION_ADD' => 'Adicionar permissão',
//</editor-fold>
//</editor-fold>
//<editor-fold desc="USER APP">
//***************************//
// USER APP
//**************************//
'USER_APP_TITLE' => 'Usuários do APP',
//</editor-fold>
//<editor-fold desc="TAREFAS">
//***************************//
// TAREFAS
//**************************//
'TASK_TITLE' => 'Tarefas',
'TITLE_ADD_TASK' => 'Adicionar Tarefa',
'TITLE_EDIT_TASK' => 'Editar Tarefa',
'OPEN_TASKS_TITLE' => 'Tarefas Abertas',
'WAITING_TASKS_TITLE' => 'Tarefas Aguardando Ciente',
'FINISHED_TASKS_TITLE' => 'Tarefas Encerradas',
'TASK_STATUS_LATE' => 'Atrasada',
'TASK_STATUS_ON_TIME' => 'No Prazo',
'TASK_STATUS_FUTURE' => 'Futura',
'TASK_STATUS_ENDED' => 'Encerrada',
'TASK_STATUS_WAITING_OPERATOR' => 'Aguardando Operador',
'TASK_STATUS_OPEN' => 'Aberta',
'TASK_STATUS_WAITING' => 'Aguardando Ciente',
'TASK_TABLE_LAST_PROGRESS' => '<b>[:last_progress_user:] em [:last_progress_date:] escreveu:</b><br/>',
'TASK_TABLE_NO_PROGRESS' => '<b>Nenhum andamento!</b>',
'TASK_SECTION' => 'Tarefa',
'TASK_PROGRESS_SECTION' => 'Andamentos',
'TASK_INTERVAL_TEXT' => 'Inicia em %s e finaliza em %s',
'BTN_ADD_PROGRESS' => 'Adicionar&nbsp;Andamento',
'BTN_EDIT_TASK' => 'Editar Tarefa',
'BTN_END_TASK' => 'Encerrar Tarefa',
'BTN_WAIT_TASK' => 'Aguardar&nbsp;Ciente',
'BTN_TRANSFER_TASK' => 'Transferir&nbsp;Tarefa',
'BTN_REOPEN_TASK' => 'Reabrir Tarefa',
'TITLE_ADD_TASK_PROGRESS' => 'Adicionar Andamento',
'TITLE_TRANSFER_TASK' => 'Transferir Tarefa',
'TITLE_REOPEN_TASK' => 'Reabrir Tarefa',
'TITLE_FINISH_TASK' => 'Encerrar Tarefa',
'TITLE_WAIT_TASK' => 'Aguardar Ciente',
//</editor-fold>
//</editor-fold>
//<editor-fold desc="VALIDATOR">
//============================================================================//
// VALIDATOR
//============================================================================//
'VALIDATOR_ERROR_CSRF_TITLE' => 'ERRO!',
'VALIDATOR_ERROR_CSRF_MESSAGE' => 'Erro CSRF! Algo não aconteceu como esperado, por favor recarregue a pagina!',
'VALIDATOR_EMPTY' => 'Este campo é requerido.',
'VALIDATOR_MAX_LENGTH' => 'Este campo excede o limite de caracteres.',
'VALIDATOR_EMAIL' => 'Por favor, forneça um endereço de email válido.',
'VALIDATOR_PASSWORD' => 'A senha deve ter no mínimo 8 caracteres e conter letras e números.',
'VALIDATOR_CONFIRM_PASSWORD' => 'Senhas não conferem.',
'VALIDATOR_TASK_FINAL_DATE' => 'A data limite da tarefa deve ser maior que a data inicial da tarefa.',
'VALIDATOR_DIFFERENT_EXECUTOR' => 'O executor deve ser diferente do atual.',
//</editor-fold>
);

