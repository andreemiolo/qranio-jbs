# Cerebelo 3.0 - Aplicação de gerenciamento desenvolvida pela Qranio com Slim Framework 3

## Visão geral

O **Cerebelo 3.0** é o sistema administrativo do Qranio™, funcionando em conjunto com o WS. 
Para acessá-lo, o usuário deve possuir autenticação por e-mail e senha. 
A partir de sua utilização, é possível realizar as seguintes ações:

*  Gerenciar usuários
*  Gerenciar cursos
*  Gerenciar perguntas gerais
*  Gerenciar as perguntas do dia
*  Gerenciar grupos e permissões de acesso
*  Gerenciar tarefas

Novas funcionalidades serão adicionadas em breve.

## Utilização

### Dependências

Todas as dependências do projeto estão listadas no arquivo `composer.json`, que são basicamente:

*  PHP >= 5.5.0
*  MySQL
*  [Composer](https://getcomposer.org/)
*  [Bower](https://bower.io/)
*  [Twig template engine](https://github.com/slimphp/Twig-View)
*  [monolog/monolog](https://packagist.org/packages/monolog/monolog)
*  [vlucas/phpdotenv](https://github.com/vlucas/phpdotenv)
*  [slim/csrf](https://github.com/slimphp/Slim-Csrf)
*  [hashids/hashids](https://packagist.org/packages/hashids/hashids)
*  [symfony/translation](https://packagist.org/packages/symfony/translation)
*  [symfony/twig-bride](https://github.com/symfony/twig-bridge)
*  [guzzlehttp/guzzle](http://docs.guzzlephp.org/en/stable/)
*  [slim/session](https://github.com/bryanjhv/slim-session)
*  [wixel/gump](https://github.com/Wixel/GUMP)
*  [aura/input](https://github.com/auraphp/Aura.Input)
*  [aura/html](https://github.com/auraphp/Aura.Html)
*  [sendgrid/sendgrid](https://github.com/sendgrid/sendgrid-php)


Consulte sempre a [documentação oficial](https://www.slimframework.com/) para mais detalhes acerca do Slim framework 3.

### Instalação

Para instalar e rodar o projeto em sua máquina, realize o clone do repositório no terminal com:

`git clone https://seu-usuario-aqui@bitbucket.org/gianmenezes/cerebelo-adm.git`

Navegue até a pasta *cerebelo-adm* que foi recém criada a partir do clone e rode o comando:

`composer install`

Isto irá baixar e configurar todas as dependências do projeto. Entretanto, não serão carregados os estilos (bootstrap, jquery, etc). Para que isso seja feito, rode o comando:

`bower install`

Isso fará com que todas as dependências gerenciadas pelo bower sejam instaladas. Você pode consultar todas estas dependências no arquivo `bower.json`

### Web server

É possível configurar o [Nginx ou Apache como servidor padrão da aplicação](https://www.slimframework.com/docs/v3/start/web-servers.html), mas o jeito mais fácil e rápido de rodar o projeto é utilizando o servidor integrado do PHP. Para isto, navegue pelo terminal até a pasta *cerebelo-adm* e execute o seguinte comando:

`php -S localhost:8888 -t public public/index.php`

Após isso basta visitar o endereço `localhost:8888` em seu navegador.

### Configurações adicionais

O projeto contém um arquivo `.env.example` na raíz. Utilize este arquivo para importar as credenciais para acesso ao banco de dados remoto.
Após configurado o arquivo, remova a extensão `.example` e reinicie o servidor.