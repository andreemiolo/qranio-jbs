var qrCerebelo = {};

qrCerebelo.block = function(opts) {
    opts = opts || {};

    $.blockUI({
        message: opts.message || null,
        fadeIn: opts.fadeIn || '0',
        css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', '-webkit-border-radius': '10px', '-moz-border-radius': '10px',  opacity: .5, 
            color: '#fff' 
        }
    });
};

qrCerebelo.unblock = function() {
    $.unblockUI();
};
qrCerebelo.scrollUp = function() {
    $("html, body").animate({
        scrollTop: 0
    }, 700);
    return false;
};

qrCerebelo.validationMessages = function(opts) {
    var _messages = [];
    _messages.push('<div class="alert">');
    _messages.push('<p>');

    $.each(opts.validations, function(i, validations) {
        _messages.push('<span><i class="fa fa-exclamation-triangle icon-1x"></i> ' + i + '</span>');
        _messages.push('<ul>');
        $.each(validations, function() {
            _messages.push('<li>' + this + '</li>');
        });
        _messages.push('</ul>');
    });

    _messages.push('</p>');
    _messages.push('</div>');



    $(opts.cx).html(_messages.join(''));
};


qrCerebelo.errorMessages = function(opts) {
    var _messages = [];
    _messages.push('<div class="alert alert-block alert-error ">\n' +
        '    <button type="button" class="close" data-dismiss="alert"></button>\n' +
        '    <h4 class="alert-heading"> {{ "ALERT_ERROR"|trans }}</h4>');
    $.each(opts.messages, function() {
        _messages.push('<p>' + this + '</p>');
    });
    _messages.push('</div>');
    $(opts.cx).html(_messages.join(''));
};

qrCerebelo.successMessages = function(opts) {
    var _messages = [];
    _messages.push('<div class="alert alert-success">');
    $.each(opts.messages, function() {
        _messages.push('<p><i class="fa fa-thumbs-up icon-1x"></i> ' + this + '</p>');
    });
    _messages.push('</div>');
    $(opts.cx).html(_messages.join(''));
};

qrCerebelo.genericError = function(opts) {
    opts = opts || {};
    qrCerebelo.errorMessages({
        messages: [
            qrDataObj.trans.SERVICE_ERROR
        ]
    });
};


qrCerebelo.loadPageGrid = function(mainclass) {

    $(mainclass).find('.ajax-btn-page').on('click', function(e) {
        e.preventDefault();
        qrCerebelo.block();
        $.ajax({
            url: $(this).attr('href'),
            dataType: 'json'
        }).done(function(r) {
            qrCerebelo.unblock();
            if (r.error && r.data && r.data.redirect) {
                window.location = r.data.next_url;
                return false;
            }
            $('#content').html(r.data.qr_content);
        });
        return false;
    });
};

//Início da Função FormataReais
documentall = document.all;
/*
* função para formatação de valores monetários retirada de
* http://jonasgalvez.com/br/blog/2003-08/egocentrismo
*/

function formatamoney(c) {
    var t = this; if(c == undefined) c = 2;		
    var p, d = (t=t.split("."))[1].substr(0, c);
    for(p = (t=t[0]).length; (p-=3) >= 1;) {
        t = t.substr(0,p) + "" + t.substr(p);
    }
    
    return t+"."+d+Array(c+1-d.length).join(0);
}

String.prototype.formatCurrency=formatamoney

function demaskvalue(valor, currency){
    /*
* Se currency é false, retorna o valor sem apenas com os números. Se é true, os dois últimos caracteres são considerados as 
* casas decimais
*/
    var val2 = '';
    var strCheck = '0123456789';
    var len = valor.length;
    if (len== 0){
        return 0.00;
    }

    if (currency ==true){
        /* Elimina os zeros à esquerda
		* a variável  <i> passa a ser a localização do primeiro caractere após os zeros e 
		* val2 contém os caracteres (descontando os zeros à esquerda)
		*/
		
        for(var i = 0; i < len; i++)
            if ((valor.charAt(i) != '0') && (valor.charAt(i) != ',')) break;
		
        for(; i < len; i++){
            if (strCheck.indexOf(valor.charAt(i))!=-1) val2+= valor.charAt(i);
        }

        if(val2.length==0) return "0.00";
        if (val2.length==1)return "0.0" + val2;
        if (val2.length==2)return "0." + val2;
		
        var parte1 = val2.substring(0,val2.length-2);
        var parte2 = val2.substring(val2.length-2);
        var returnvalue = parte1 + "." + parte2;
        return returnvalue;
		
    }
    else{
        /* currency é false: retornamos os valores COM os zeros à esquerda,
			* sem considerar os últimos 2 algarismos como casas decimais 
			*/
        val3 ="";
        for(var k=0; k < len; k++){
            if (strCheck.indexOf(valor.charAt(k))!=-1) val3+= valor.charAt(k);
        }
        return val3;
    }
}

function reais(obj,event){

    var whichCode = (window.Event) ? event.which : event.keyCode;
    /*
Executa a formatação após o backspace nos navegadores !document.all
*/
    if (whichCode == 8 && !documentall) {
        /*
Previne a ação padrão nos navegadores
*/
        if (event.preventDefault){ //standart browsers
            event.preventDefault();
        }else{ // internet explorer
            event.returnValue = false;
        }
        var valor = obj.value;
        var x = valor.substring(0,valor.length-1);
        obj.value= demaskvalue(x,true).formatCurrency();
        return false;
    }
    /*
Executa o Formata Reais e faz o format currency novamente após o backspace
*/
    FormataReais(obj,'.',',',event);
} // end reais


function backspace(obj,event){
    /*
Essa função basicamente altera o  backspace nos input com máscara reais para os navegadores IE e opera.
O IE não detecta o keycode 8 no evento keypress, por isso, tratamos no keydown.
Como o opera suporta o infame document.all, tratamos dele na mesma parte do código.
*/

    var whichCode = (window.Event) ? event.which : event.keyCode;
    if (whichCode == 8 && documentall) {
        var valor = obj.value;
        var x = valor.substring(0,valor.length-1);
        var y = demaskvalue(x,true).formatCurrency();

        obj.value =""; //necessário para o opera
        obj.value += y;
	
        if (event.preventDefault){ //standart browsers
            event.preventDefault();
        }else{ // internet explorer
            event.returnValue = false;
        }
        return false;

    }// end if
}// end backspace

function FormataReais(fld, milSep, decSep, e) {
    var sep = 0;
    var key = '';
    var i = j = 0;
    var len = len2 = 0;
    var strCheck = '0123456789';
    var aux = aux2 = '';
    var whichCode = (window.Event) ? e.which : e.keyCode;

    //if (whichCode == 8 ) return true; //backspace - estamos tratando disso em outra função no keydown
    if (whichCode == 0 ) return true;
    if (whichCode == 9 ) return true; //tecla tab
    if (whichCode == 13) return true; //tecla enter
    if (whichCode == 16) return true; //shift internet explorer
    if (whichCode == 17) return true; //control no internet explorer
    if (whichCode == 27 ) return true; //tecla esc
    if (whichCode == 34 ) return true; //tecla end
    if (whichCode == 35 ) return true;//tecla end
    if (whichCode == 36 ) return true; //tecla home

    /*
O trecho abaixo previne a ação padrão nos navegadores. Não estamos inserindo o caractere normalmente, mas via script
*/

    if (e.preventDefault){ //standart browsers
        e.preventDefault()
    }else{ // internet explorer
        e.returnValue = false
    }

    var key = String.fromCharCode(whichCode);  // Valor para o código da Chave
    if (strCheck.indexOf(key) == -1) return false;  // Chave inválida

    /*
Concatenamos ao value o keycode de key, se esse for um número
*/
    fld.value += key;

    var len = fld.value.length;
    var bodeaux = demaskvalue(fld.value,true).formatCurrency();
    fld.value=bodeaux;

    /*
Essa parte da função tão somente move o cursor para o final no opera. Atualmente não existe como movê-lo no konqueror.
*/
    if (fld.createTextRange) {
        var range = fld.createTextRange();
        range.collapse(false);
        range.select();
    }
    else if (fld.setSelectionRange) {
        fld.focus();
        var length = fld.value.length;
        fld.setSelectionRange(length, length);
    }
    return false;

}
//Fim da Função FormataReais
function mascara(o,f){
    v_obj=o
    v_fun=f
    setTimeout("execmascara()",1)
}

function execmascara(){
    v_obj.value=v_fun(v_obj.value)
}


function soLetras(v){
    return v.replace(/\d/g,"") //Remove tudo o que não é Letra
}

function soLetrasMA(v){
    v=v.toUpperCase() //Maiúsculas
    return v.replace(/\d/g,"") //Remove tudo o que não é Letra ->maiusculas
}

function soLetrasMI(v){
    v=v.toLowerCase() //Minusculas
    return v.replace(/\d/g,"") //Remove tudo o que não é Letra ->minusculas
}

function mascaraLogin(v){
    v=v.toLowerCase() //Minusculas
    return v.replace(/[^a-z._0-9]/g,"") //Remove tudo o que não é Letra ->minusculas
}

function soNumeros(v){
    return v.replace(/\D/g,"") //Remove tudo o que não é dígito
}

function soValorMonetario(v){
    v=v.replace(/\D/g,"") //Remove tudo o que não é dígito
    v=v.replace(/(\d{1})(\d{1,2})$/,"$1.$2") //Remove tudo o que não é dígito
    return v
}

function apenastelefone(v){
    v=v.replace(/\D/g,"") //Remove tudo o que não é dígito
    v=v.replace(/^(\d\d)(\d)/g,"($1) $2") //Coloca parênteses em volta dos dois primeiros dígitos
    v=v.replace(/(\d{4})(\d)/,"$1-$2") //Coloca hífen entre o quarto e o quinto dígitos
    return v
}

function cpfvalidar(v){
    v=v.replace(/\D/g,"") //Remove tudo o que não é dígito
    v=v.replace(/(\d{3})(\d)/,"$1.$2") //Coloca um ponto entre o terceiro e o quarto dígitos
    v=v.replace(/(\d{3})(\d)/,"$1.$2") //Coloca um ponto entre o terceiro e o quarto dígitos
    //de novo (para o segundo bloco de números)
    v=v.replace(/(\d{3})(\d{1,2})$/,"$1-$2") //Coloca um hífen entre o terceiro e o quarto dígitos
    return v
}

function cepvalidar(v){
    v=v.replace(/\D/g,"") //Remove tudo o que não é dígito
    v=v.replace(/^(\d{5})(\d)/,"$1-$2") //Esse é tão fácil que não merece explicações
    return v
}

function cnpjvalidar(v){
    v=v.replace(/\D/g,"") //Remove tudo o que não é dígito
    v=v.replace(/^(\d{2})(\d)/,"$1.$2") //Coloca ponto entre o segundo e o terceiro dígitos
    v=v.replace(/^(\d{2})\.(\d{3})(\d)/,"$1.$2.$3") //Coloca ponto entre o quinto e o sexto dígitos
    v=v.replace(/\.(\d{3})(\d)/,".$1/$2") //Coloca uma barra entre o oitavo e o nono dígitos
    v=v.replace(/(\d{4})(\d)/,"$1-$2") //Coloca um hífen depois do bloco de quatro dígitos
    return v
}

function romanos(v){
    v=v.toUpperCase() //Maiúsculas
    v=v.replace(/[^IVXLCDM]/g,"") //Remove tudo o que não for I, V, X, L, C, D ou M
    //Essa é complicada! Copiei daqui: http://www.diveintopython.org/refactoring/refactoring.html
    while(v.replace(/^M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$/,"")!="")
        v=v.replace(/.$/,"")
    return v
}

function data(v){
    v=v.replace(/\D/g,"") //Remove tudo o que não é dígito
    v=v.replace(/(\d{2})(\d)/,"$1/$2") //Coloca um ponto entre o segundo e o terceiro dígitos
    v=v.replace(/(\d{2})(\d)/,"$1/$2") //Coloca um ponto entre o quarto e o quinto dígitos
    //v=v.replace(/^[0-3]?\d\/[01]?\d\/(\d{2}|\d{4})$/)
    v=v.replace(/^((0[1-9]|[12]\d)\-(0[1-9]|1[0-2])|30\-(0[13-9]|1[0-2])|31\-(0[13578]|1[02]))\-\d{4}$/)
    //v=v.replace(/^(0[1-9]|[012][0-9]|3[01])/\-(0[1-9]|1[012])/\-([12][0-9]{3})/,"")
    return v
}


//valida formato de hora 00:00 até 23:59 com mascara
//criada em 2008-04-15 15:57
//er=/^(([01][\d])|([2][0-3]))([0-5][\d])/ //ereg que valida a hora(nao usada aqui)
function hora(v){
    v=v.replace(/\D/g,"") //Remove tudo o que não é dígito
    v=v.replace(/^[^012]/,"") //valida o primeiro dígito #
    v=v.replace(/^([2])([^0-3])/,"$1") //valida o segundo dígito ##
    v=v.replace(/^([\d]{2})([^0-5])/,"$1")//valida o terceiro dígito ###
    v=v.replace(/(\d{2})(\d)/,"$1:$2") //Coloca dois ponto entre o segundo e o terceiro dígitos ##:##
    v=v.substr(0,5) //Remove digitos extras (aceita no max 5 caracteres(contando o ':' no meio) )
    return v
}

function site(v){
    //Esse sem comentarios para que você entenda sozinho ;-)
    v=v.replace(/^http:\/\/?/,"")
    dominio=v
    caminho=""
    if(v.indexOf("/")>-1)
        dominio=v.split("/")[0]
    caminho=v.replace(/[^\/]*/,"")
    dominio=dominio.replace(/[^\w\.\+-:@]/g,"")
    caminho=caminho.replace(/[^\w\d\+-@:\?&=%\(\)\.]/g,"")
    caminho=caminho.replace(/([\?&])=/,"$1")
    if(caminho!="")dominio=dominio.replace(/\.+$/,"")
    v="http://"+dominio+caminho
    return v
}