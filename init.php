<?php

// Definindo TimeZone 
date_default_timezone_set('America/Sao_Paulo');

// Definindo Diretório Raiz do Projeto
define('QR_DIR_BASE', strtr(__DIR__, '\\', '/'));


//Carregando autoloads
require QR_DIR_BASE . '/vendor/autoload.php';

// Carregando o .env
if(file_exists(QR_DIR_BASE . '/.env')) {
    $dotenv = new Dotenv\Dotenv(QR_DIR_BASE);
    $dotenv->load();
}

// Instantiate the app
$settings = require QR_DIR_BASE . '/App/Http/Config/Settings.php';

define('QR_ENV', getenv('QR_ENV') ?: 'development');

$app = new \Slim\App($settings);

// Set up dependencies
require QR_DIR_BASE . '/App/Http/Dependencie/Dependencies.php';

// Register middleware
require QR_DIR_BASE . '/App/Http/Middleware/Middleware.php';

// Register routes
require QR_DIR_BASE . '/App/Http/Routes.php';

// Set up dependencies
require QR_DIR_BASE . '/App/Http/Dependencie/Functions.php';

// Run app
$app->run();




